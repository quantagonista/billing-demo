from dataclasses import dataclass
from typing import Optional

from dateutil.relativedelta import relativedelta
from django.db import transaction
from django.http import HttpRequest

from core import constants as const
from core.models import LicenseEntity
from core.services import CustomerService, LicenseService
from payments.services import PaymentService


@dataclass
class InstantPayResult:
    redirect_url: Optional[str] = None
    message: Optional[str] = None


class InstantPayService:
    @classmethod
    @transaction.atomic
    def init_payment(cls, request: HttpRequest, data: dict, payment_provider: str) -> InstantPayResult:
        name, email, start, end, price = data['name'], data['email'], data['start'], data['end'], data['price']

        customer = CustomerService.create_customer(name=name, contact_email=email, users_count=1)
        first_payment_months = relativedelta(end, start).months

        license_, invoice = LicenseService.create_license_with_first_invoice(
            customer=customer, license_entity=LicenseEntity.get_default(),
            start_date=start, price=price, first_payment=price,
            first_payment_months=first_payment_months
        )

        description = f'Instant Subscription:({customer.id}) {customer.name}'

        result, order = PaymentService.init_payment(
            amount=price,
            description=description,
            request=request,
            customer=customer,
            invoice=invoice,
            provider_name=payment_provider,
        )

        if result.redirect_url:
            return InstantPayResult(redirect_url=result.redirect_url)

        if result.error_description:
            return InstantPayResult(message=f"({result.error_code}){result.error_description}")

        return InstantPayResult(None, const.INVALID_PAYMENT_PROVIDER_ERROR)
