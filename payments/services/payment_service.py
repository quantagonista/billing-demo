import logging
from decimal import Decimal

from django.db import transaction, IntegrityError
from django.http import HttpRequest
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from django.conf import settings
from core.choices import PaymentOrderStatuses
from core.exceptions import InitPaymentError
from core.models import Customer, Transaction, PaymentMethod
from core.services.license import LicenseCheckService
from .. import exceptions
from ..models import PaymentOrder, Currency
from ..providers import PayboxProvider, StripePaymentProvider

logger = logging.getLogger("django")


paybox_conf = (
    settings.PAYBOX_MERCHANT_ID,
    settings.PAYBOX_PRIVATE_KEY,
    settings.SECRET_KEY,
    settings.PAYBOX_TEST_MODE,
    settings.SITE_URL,
)

stripe_conf = (
    settings.STRIPE_API_KEY,
    settings.STRIPE_API_VERSION,
    settings.SITE_URL
)

PROVIDERS = {
    'paybox': PayboxProvider(*paybox_conf),
    'stripe': StripePaymentProvider(*stripe_conf),
}


class PaymentService:
    @classmethod
    @transaction.atomic
    def init_payment(cls,
                     request: HttpRequest,
                     customer: Customer,
                     amount: Decimal,
                     description: str,
                     provider_name: str, **kwargs):

        provider = cls.get_provider(provider_name)

        currency, __ = Currency.objects.get_or_create(code="USD", defaults={"name": _("United States Dollar")})

        invoice = kwargs.pop('invoice') if kwargs.get('invoice') else None
        initiator = request.user if request.user.is_authenticated else None

        order = PaymentOrder.objects.create(
            customer=customer,
            initiator=initiator,
            currency=currency,
            amount=amount,
            description=description,
            payment_system=provider_name,
            invoice=invoice
        )

        result = provider.init_payment(
            currency=currency,
            order=order,
            amount=amount,
            description=description,
            **kwargs
        )

        if not result:
            raise InitPaymentError()

        if result.payment_id:
            order.external_order_id = result.payment_id
            order.status = "in_processing"
            order.save()

        return result, order

    @classmethod
    def refresh_payment_status(cls, order: PaymentOrder) -> PaymentOrder:
        if order.has_end_status():
            return order

        try:
            return cls.update_payment_status(order)

        except Exception as e:
            logger.exception(f"Cannot update payment status. Reason: {e}")
            return order

    @classmethod
    @transaction.atomic()
    def update_payment_status(cls, order: PaymentOrder, status: str = None) -> PaymentOrder:
        provider = cls.get_provider(order.payment_system)

        result = provider.get_payment_status(payment_id=order.external_order_id, status=status)

        if result.payment_status and order.status != result.payment_status:
            order.status = result.payment_status

            if not order.payment_date and order.has_end_status():
                order.payment_date = timezone.now()

            if order.status == PaymentOrderStatuses.SUCCESS:
                try:
                    cls.create_transactions_for_order(order)
                    LicenseCheckService.check_license_and_update_crm_for(order.customer)
                except IntegrityError:
                    logger.error(f'Could not create transactions for order {order.id}. Because they already exists')

            order.save()
        return order

    @staticmethod
    def get_provider(provider_name):
        provider = PROVIDERS.get(provider_name)

        if not provider:
            raise exceptions.InvalidPaymentProvider()

        return provider

    @classmethod
    def create_transactions_for_order(cls, order: PaymentOrder):
        payment_method = cls._get_payment_method_from_order(order)

        debit_transaction = Transaction.objects.create(
            customer=order.customer,
            payment_method=payment_method,
            type=Transaction.DEBIT,
            amount=order.amount,
        )

        credit_transaction = Transaction.objects.create(
            customer=order.customer,
            payment_method=payment_method,
            type=Transaction.CREDIT,
            amount=order.amount,
            invoice=order.invoice
        )

        order.transaction = credit_transaction

        order.save()

    @classmethod
    @transaction.atomic()
    def finalize_order(cls, order: PaymentOrder, error=False, err_code='', err_desc='', message=''):
        if error:
            order.save_as_error(err_code, err_desc)
            return order

        order.save_as_success(message)
        try:
            cls.create_transactions_for_order(order)
            LicenseCheckService.check_license_and_update_crm_for(order.customer)
        except IntegrityError:
            logger.error(f'Could not create transactions for order {order.id}. Because they already exists')

        return order

    @classmethod
    def _get_payment_method_from_order(cls, order: PaymentOrder):
        payment_method_name = str(order.payment_system).capitalize()
        payment_method, _ = PaymentMethod.objects.get_or_create(name=payment_method_name)

        return payment_method
