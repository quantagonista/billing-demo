from core.exceptions import BaseError

from . import constants as const


class ImproperlyConfigured(BaseError):
    default_message = const.IMPROPERLY_CONFIGURED_ERROR


class InvalidPaymentProvider(BaseError):
    default_message = const.INVALID_PAYMENT_PROVIDER
