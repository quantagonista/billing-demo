from django import forms

from core import constants as const


class InstantPayForm(forms.Form):
    name = forms.CharField(label=const.CUSTOMER_NAME)
    email = forms.EmailField(label=const.EMAIL)
    start = forms.DateField(label=const.START_DATE)
    end = forms.DateField(label=const.END_DATE)
    price = forms.IntegerField(label=const.PRICE, min_value=20)

