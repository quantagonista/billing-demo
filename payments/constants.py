from django.utils.translation import gettext_lazy as _

IMPROPERLY_CONFIGURED_ERROR = _('Неправильная конфигурация системы')
INVALID_PAYMENT_PROVIDER = _('В данный момент система не поддерживает данного провайдера')

# Paybox
INVALID_REQUEST_CONFIGURATION_ERROR = _('Для вызова данного метода необходимо проинициализировать данные. Вызвать .validate_kwargs()')
