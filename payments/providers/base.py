import abc


class BasePaymentProvider(abc.ABC):

    @abc.abstractmethod
    def init_payment(self, **kwargs):
        pass

    @abc.abstractmethod
    def get_payment_status(self, **kwargs):
        pass
