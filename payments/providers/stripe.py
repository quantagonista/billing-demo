import logging

import stripe
import stripe.error
from django.urls import reverse

from payments.entities.stripe import StripePaymentRequest, StripePaymentStatusResult
from payments.providers import BasePaymentProvider

logger = logging.getLogger(__name__)


class StripePaymentProvider(BasePaymentProvider):
    def __init__(self, api_key: str, api_version: str, base_redirect_url: str):
        self.base_redirect_url = base_redirect_url
        self.api_version = api_version
        self.api_key = api_key

        self.init_stripe()

    def init_stripe(self):
        stripe.api_key = self.api_key
        stripe.api_version = self.api_version

    def init_payment(self, currency, order, amount, description, **kwargs) -> StripePaymentRequest:
        result_url = f"{self.base_redirect_url}{reverse('stripe_result')}"

        success_url = result_url + f'?order={order.id}&status=succeeded'
        cancel_url = result_url + f'?order={order.id}&status=canceled'

        try:
            checkout_session = stripe.checkout.Session.create(
                mode='payment',
                locale='auto',
                success_url=success_url,
                cancel_url=cancel_url,
                payment_method_types=['card'],
                payment_intent_data={
                    'description': f"{order.customer}: {order.invoice.title}"
                },
                line_items=[{
                    'price_data': {
                        'currency': currency.code,
                        'product_data': {
                            'name': order.invoice.title,
                        },
                        'unit_amount': int(amount * 100)
                    },
                    'quantity': 1
                }],
            )

            return StripePaymentRequest(checkout_session.payment_intent, checkout_session.url)

        except stripe.error.StripeError as e:
            logger.exception(str(e), exc_info=e)

    def get_payment_status(self, payment_id: str, status: str = None) -> StripePaymentStatusResult:
        if not status:
            try:
                result = stripe.PaymentIntent.retrieve(payment_id)
                status = result.status

            except stripe.error.StripeError as e:
                logger.exception(str(e), exc_info=e)

        return StripePaymentStatusResult(self._convert_status(status))

    @staticmethod
    def _convert_status(status: str) -> str:
        return {
            'created': 'new',
            'succeeded': 'success',
            'canceled': 'canceled',
            'requires_action': 'in_processing',
            'processing': 'in_processing',
            'payment_failed': 'error',
            'requires_payment_method': 'in_processing',

            # webhook events
            'payment_intent.succeeded': 'success',
            'payment_intent.canceled': 'canceled',
            'payment_intent.payment_failed': 'error',

        }.get(status, None)
