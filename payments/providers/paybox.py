import logging
import xml.etree.ElementTree as ET
from hashlib import md5

import requests
from django.urls import reverse
from django.utils.text import slugify
from requests import Response

from . import BasePaymentProvider
from .. import entities

logger = logging.getLogger(__name__)


class PayboxProvider(BasePaymentProvider):
    base_url = 'https://api.paybox.money/'

    def __init__(self, merchant_id: str, private_key: str, salt: str, testing_mode: bool, base_redirect_url: str):
        self.merchant_id = merchant_id
        self.private_key = private_key
        self.salt = salt
        self.testing_mode = int(testing_mode)
        self.base_redirect_url = base_redirect_url

    def init_payment(self, **kwargs) -> entities.PayboxPaymentInitResult:
        paybox_request = self._validate_kwargs(entities.PayboxPaymentRequest, **self._update_kwargs(kwargs))

        request_params = self._get_payment_init_params(paybox_request)

        response = requests.get(url=self.base_url + 'init_payment.php', params=request_params)
        self._log_request(request_params, response)

        if not response.status_code == 200:
            return self._handle_error(entities.PayboxPaymentInitResult, response)

        return self._handle_success(response)

    def get_payment_status(self, payment_id: str, status: str = None):
        paybox_request = self._validate_kwargs(entities.PayboxPaymentStatusRequest, payment_id=payment_id)

        params = self.get_payment_status_params(paybox_request)

        url = self.base_url + 'get_status.php'

        response = requests.get(url, params)
        self._log_request(params, response)

        if not response.status_code == 200:
            return self._handle_error(entities.PayboxPaymentStatusResult, response)

        return self._handle_payment_status_response(response)

    def _get_signature(self, params: dict, method: str):
        data = f"{method};"

        for key in sorted(list(params.keys())):
            data += f"{params[key]};"

        data += self.private_key

        return md5(bytes(data, 'utf-8')).hexdigest()

    def _get_payment_init_params(self, request: entities.PayboxPaymentRequest):
        description = self._process_description(request.description)

        params = {
            "pg_amount": request.amount,
            "pg_check_url": request.check_url,
            "pg_failure_url": request.status_url,
            "pg_site_url": request.status_url,
            "pg_success_url": request.status_url,
            "pg_testing_mode": request.testing_mode,
            "pg_currency": request.currency,
            "pg_description": description,
            "pg_merchant_id": request.merchant_id,
            "pg_order_id": request.order_id,
            "pg_request_method": "POST",
            "pg_result_url": request.result_url,
            "pg_salt": request.salt,
        }

        signature = self._get_signature(params, 'init_payment.php')
        params["pg_sig"] = signature

        return params

    @staticmethod
    def _log_request(params: dict, response: Response):
        logger.info(f"GET: {response.url}")
        logger.info(f"Params: {str(params)}")
        logger.info(f"Result URL: {str(params.get('pg_result_url', ''))}")
        logger.info(f"Status: {str(response.status_code)}")
        logger.info(f"Response: {str(response.text)}")

    def _validate_kwargs(self, validation_entity, **kwargs):
        return validation_entity(merchant_id=self.merchant_id, salt=self.salt, testing_mode=self.testing_mode, **kwargs)

    @staticmethod
    def _handle_error(entity, response: requests.Response):
        return entity(error_description=response.text)

    @staticmethod
    def _handle_success(response: requests.Response) -> entities.PayboxPaymentInitResult:
        root = ET.fromstring(response.text)
        status = root.find("pg_status").text

        if status == "ok":
            payment_id = root.find("pg_payment_id").text
            redirect_url = root.find("pg_redirect_url").text
            status = "in_processing"
            return entities.PayboxPaymentInitResult(payment_id=payment_id, payment_status=status, redirect_url=redirect_url)

        error_code = root.find("pg_error_code")
        error_description = root.find("pg_error_description")

        error_code = error_code.text if error_code else 0
        error_description = error_description.text if error_description else ''

        return entities.PayboxPaymentInitResult(error_code=error_code, error_description=error_description)

    def get_payment_status_params(self, request: entities.PayboxPaymentStatusRequest):
        params = {
            "pg_merchant_id": request.merchant_id,
            "pg_payment_id": request.payment_id,
            "pg_salt": request.salt,
        }
        signature = self._get_signature(params, "get_status.php")
        params["pg_sig"] = signature

        return params

    def _handle_payment_status_response(self, response: requests.Response) -> entities.PayboxPaymentStatusResult:
        root = ET.fromstring(response.text)
        status = root.find("pg_status").text
        error = root.find("pg_error_description").text


        if not status == 'ok':
            return entities.PayboxPaymentStatusResult(status, error)

        transaction_status = root.find("pg_transaction_status").text
        transaction_status = self._convert_status(transaction_status)

        return entities.PayboxPaymentStatusResult(payment_status=transaction_status)

    @staticmethod
    def _convert_status(status: str) -> str:
        return {
            "partial": "in_processing",
            "pending": "in_processing",
            "ok": "success",
            "failed": "error",
            "revoked": "revoked",
            "refunded": "canceled",
            "incomplete": "error"
        }.get(status, None)

    def _update_kwargs(self, kwargs: dict) -> dict:
        order = kwargs.pop('order') if kwargs.get('order') else None
        order_id = getattr(order, 'id', '')

        status_url = self.base_redirect_url + reverse("payment_status_view", kwargs={"pk": order_id})
        check_url = self.base_redirect_url + reverse("paybox_check")
        result_url = self.base_redirect_url + reverse("paybox_result")

        kwargs.update({
            'check_url': check_url,
            'result_url': result_url,
            'failure_url': status_url,
            'status_url': status_url,
            'site_url': status_url,
            'order_id': order_id,
        })

        return kwargs

    def _process_description(self, description: str):
        description = slugify(description).replace("-", " ")

        disallowed_words = [" etc ", " conf ", " env "]
        for word in disallowed_words:
            description = description.replace(word, " ")

        description = f"Billing {description}"
        if self.testing_mode:
            description = f"Billing Test Mode {description}"

        return description
