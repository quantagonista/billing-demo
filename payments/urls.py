from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path('init-payment', views.InitPaymentView.as_view(), name='payment_init_view'),
    path('order/<int:pk>/status', views.PaymentStatusView.as_view(), name='payment_status_view'),

    path('paybox/check', csrf_exempt(views.PayboxCheckPaymentView.as_view()), name='paybox_check'),
    path('paybox/result', csrf_exempt(views.PayboxPaymentResultView.as_view()), name='paybox_result'),

    path('stripe/result', csrf_exempt(views.StripePaymentResultView.as_view()), name='stripe_result'),
    path('stripe/webhook', csrf_exempt(views.StripeWebhookView.as_view()), name='stripe_webhook'),

    path('payment-orders', views.PaymentOrderListView.as_view(), name='payment_order_list')
]
