# Generated by Django 5.0 on 2023-12-23 20:10

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('core', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Currency',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=10, verbose_name='Код')),
                ('name', models.CharField(max_length=100, verbose_name='Название')),
            ],
            options={
                'verbose_name': 'Валюта',
                'verbose_name_plural': 'Валюты',
            },
        ),
        migrations.CreateModel(
            name='PaymentOrder',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Дата обновления')),
                ('amount', models.DecimalField(decimal_places=2, max_digits=15, verbose_name='Сумма')),
                ('description', models.CharField(blank=True, max_length=1000, null=True, verbose_name='Описание')),
                ('status', models.CharField(choices=[('new', 'Новый'), ('in_processing', 'В обработке'), ('revoked', 'Отозван'), ('success', 'Успешный'), ('error', 'Ошибка'), ('canceled', 'Отменен')], default='new', max_length=20, verbose_name='Статус')),
                ('message', models.TextField(blank=True, null=True, verbose_name='Сообщение(об ошибке)')),
                ('payment_system', models.CharField(choices=[('paybox', 'Paybox'), ('stripe', 'Stripe'), ('common_api', 'Общий платежный шлюз')], max_length=50, verbose_name='Платежная система')),
                ('external_order_id', models.CharField(blank=True, max_length=255, null=True, verbose_name='Внешний ID платежа')),
                ('payment_date', models.DateTimeField(blank=True, null=True, verbose_name='Дата совершения платежа')),
                ('expired_date', models.DateTimeField(blank=True, null=True, verbose_name='Дата истечения')),
                ('currency', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='orders', to='payments.currency', verbose_name='Валюта')),
                ('customer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='payment_orders', to='core.customer', verbose_name='Организация')),
                ('initiator', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='my_orders', to=settings.AUTH_USER_MODEL, verbose_name='Инициатор')),
                ('invoice', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='payment_orders', to='core.invoice', verbose_name='Счет на оплату')),
                ('transaction', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='core.transaction', verbose_name='Транзакция')),
            ],
            options={
                'verbose_name': 'Платежное поручение',
                'verbose_name_plural': 'Платежные поручения',
                'ordering': ['-created_at'],
            },
        ),
    ]
