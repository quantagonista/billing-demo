from django.contrib.auth import get_user_model
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from common.models import TimestampedModel, NULLABLE
from core.choices import PaymentOrderStatuses, PaymentSystems
from core.models import Customer, Transaction, Invoice
from ..models import Currency

User = get_user_model()


class PaymentOrder(TimestampedModel):
    currency = models.ForeignKey(Currency, verbose_name=_("Валюта"), on_delete=models.PROTECT, related_name="orders")
    customer = models.ForeignKey(Customer, verbose_name=_("Организация"), on_delete=models.CASCADE, related_name="payment_orders")
    initiator = models.ForeignKey(User, verbose_name=_("Инициатор"), on_delete=models.SET_NULL, related_name="my_orders", **NULLABLE)
    transaction = models.ForeignKey(Transaction, verbose_name=_('Транзакция'), on_delete=models.PROTECT, **NULLABLE)
    invoice = models.ForeignKey(Invoice, models.PROTECT, **NULLABLE, verbose_name=_('Счет на оплату'), related_name='payment_orders')

    amount = models.DecimalField(verbose_name=_("Сумма"), max_digits=15, decimal_places=2)
    description = models.CharField(verbose_name=_("Описание"), max_length=1000, **NULLABLE)
    status = models.CharField(verbose_name=_("Статус"), max_length=20, choices=PaymentOrderStatuses.choices, default="new")
    message = models.TextField(verbose_name=_("Сообщение(об ошибке)"), **NULLABLE)

    payment_system = models.CharField(verbose_name=_("Платежная система"), choices=PaymentSystems.choices, max_length=50)
    external_order_id = models.CharField(verbose_name=_("Внешний ID платежа"), max_length=255, **NULLABLE)
    payment_date = models.DateTimeField(verbose_name=_("Дата совершения платежа"), **NULLABLE)
    expired_date = models.DateTimeField(verbose_name=_("Дата истечения"), **NULLABLE)

    def has_end_status(self):
        return self.status in {
            PaymentOrderStatuses.SUCCESS,
            PaymentOrderStatuses.ERROR,
            PaymentOrderStatuses.REVOKED,
            PaymentOrderStatuses.CANCELED
        }

    def save_as_error(self, error_code: str, error_description: str):
        self.payment_date = timezone.now()
        self.status = "error"
        self.message = f"Не удалось провести платеж в Paybox. Код: {error_code}. Ошибка: {error_description}"

        self.save()

    def save_as_success(self, message):
        self.payment_date = timezone.now()
        self.status = "success"
        self.message = message

        self.save()

    class Meta:
        ordering = ["-created_at"]
        verbose_name_plural = _("Платежные поручения")
        verbose_name = _("Платежное поручение")
