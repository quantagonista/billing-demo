from django.db import models
from django.utils.translation import gettext_lazy as _


class Currency(models.Model):
    code = models.CharField(max_length=10, verbose_name=_("Код"))
    name = models.CharField(max_length=100, verbose_name=_("Название"))

    def __str__(self):
        return self.code

    class Meta:
        verbose_name_plural = _("Валюты")
        verbose_name = _("Валюта")
