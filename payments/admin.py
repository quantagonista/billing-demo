from django.contrib import admin

# Register your models here.
from django.contrib.admin import ModelAdmin

from payments.models import PaymentOrder


@admin.register(PaymentOrder)
class PaymentOrderAdmin(ModelAdmin):
    list_display = ["id", "customer", "amount"]