import dataclasses
import decimal


@dataclasses.dataclass
class PayboxPaymentRequest:
    amount: decimal.Decimal
    site_url: str
    check_url: str
    failure_url: str
    result_url: str
    status_url: str
    currency: str
    description: str
    merchant_id: str
    order_id: str
    salt: str
    testing_mode: int = 0


@dataclasses.dataclass
class PayboxPaymentInitResult:
    payment_id: str = None
    payment_status: str = None
    redirect_url: str = None
    error_code: str = None
    error_description: str = None


@dataclasses.dataclass
class PayboxPaymentStatusRequest:
    merchant_id: str
    payment_id: str
    salt: str
    testing_mode: int = 0


@dataclasses.dataclass
class PayboxPaymentStatusResult:
    payment_status: str = None
    error_description: str = None
