import dataclasses


@dataclasses.dataclass
class StripePaymentRequest:
    payment_id: str
    redirect_url: str


@dataclasses.dataclass
class StripePaymentStatusResult:
    payment_status: str = None
    error_description: str = None
