from django.http import Http404

from common.views import CustomTemplateView
from .. import models


class PaymentOrderListView(CustomTemplateView):
    template_name = 'payments/payment_order/list.html'

    def dispatch(self, request, *args, **kwargs):
        raise Http404()

    def update_context(self, context):
        context['orders'] = models.PaymentOrder.objects.filter(initiator=self.request.user)
