from django.shortcuts import redirect
from django.views.generic import FormView

from common.views import CustomFormView
from payments.forms import InstantPayForm
from payments.services.instant_payment import InstantPayService


class InstantPayView(FormView):
    template_name = 'core/instant_pay.html'
    form_class = InstantPayForm

    def form_valid(self, form):
        result = InstantPayService.init_payment(
            self.request, form.cleaned_data, self.request.POST.get('method')
        )
        if result.redirect_url:
            return redirect(result.redirect_url)
        return redirect('/')
