from decimal import Decimal

from django.contrib import messages
from django.http import Http404
from django.shortcuts import redirect, get_object_or_404, render
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from common.views import CustomTemplateView
from core.choices import PaymentOrderStatuses
from core.exceptions import InitPaymentError
from core.models import Customer, Invoice
from ..models import PaymentOrder
from ..services import PaymentService


class InitPaymentView(CustomTemplateView):
    template_name = "payments/forms/init_payment.html"

    def dispatch(self, request, *args, **kwargs):
        description = request.GET.get("description", None)
        amount = Decimal(request.GET.get("amount", 0))
        invoice_pk = request.GET.get('invoice')
        provider = request.GET.get('provider')

        customer = Customer.objects.filter(pk=int(request.GET.get('customer', '0'))).first()

        if not customer:
            messages.warning(request, _('Введены не верные данные о клиенте'))
            return redirect(reverse('home'))

        invoice = Invoice.objects.filter(pk=invoice_pk).first()

        if not invoice:
            messages.warning(request, _('Введены не верные данные о счете'))
            return redirect(reverse('home'))

        if not (description and amount):
            messages.warning(request, _('Введены не верные данные о платеже'))
            return redirect(reverse('home'))

        if PaymentOrder.objects.filter(invoice=invoice, status=PaymentOrderStatuses.SUCCESS).exists():
            messages.warning(request, _('Данный чек уже оплачивался'))
            return redirect(reverse('home'))

        try:
            result, order = PaymentService.init_payment(
                amount=amount,
                description=description,
                request=request,
                customer=customer,
                invoice=invoice,
                provider_name=provider,
            )

            request.session["saved_payment_order"] = order.id

        except InitPaymentError as e:
            messages.warning(request, e.message)
            return redirect(reverse('home'))

        if result.redirect_url:
            return redirect(result.redirect_url)

        if result.error_description:
            messages.warning(request, f"Код:{result.error_code} Описание: {result.error_description}")
            return redirect(reverse('home'))

        messages.success(request, _('Ваш платеж принят в обработку'))
        return redirect(reverse('payment_status_view', kwargs={'pk': order.pk}))


class PaymentStatusView(CustomTemplateView):
    template_name = "payments/payment_success.html"

    def dispatch(self, request, *args, **kwargs):
        order = get_object_or_404(PaymentOrder, pk=kwargs["pk"])
        saved_payment_order_id = request.session.get("saved_payment_order", None)

        if not (request.user.is_superuser or order.initiator == request.user or saved_payment_order_id == order.id):
            raise Http404()

        order = PaymentService.refresh_payment_status(order)

        return render(request, self.template_name, {'order': order})
