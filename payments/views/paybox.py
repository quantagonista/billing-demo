import logging
from decimal import Decimal

from django.http import HttpResponse
from django.views import View

from ..models import PaymentOrder
from ..services import PaymentService

logger = logging.getLogger("django")


class PayboxResponseMixin:
    @staticmethod
    def return_ok():
        return HttpResponse(
            content_type="application/xml",
            content='<?xml version="1.0" encoding="UTF-8"?><pg_status>ok</pg_status>'
        )

    @staticmethod
    def return_rejected():
        return HttpResponse(
            content_type="application/xml",
            content='<?xml version="1.0" encoding="UTF-8"?><pg_status>rejected</pg_status>'
        )

    @staticmethod
    def return_error(description):
        return HttpResponse(
            content_type="application/xml",
            content='<?xml version="1.0" encoding="UTF-8"?>'
                    '<pg_status>error</pg_status>'
                    f'<pg_error_description>{description}</pg_error_description>'
        )


class PayboxCheckPaymentView(View, PayboxResponseMixin):
    def post(self, request, *args, **kwargs):
        order_id = request.POST.get('pg_order_id', "")
        external_order_id = request.POST.get('pg_payment_id', "")
        amount = Decimal(request.POST.get('pg_amount', 0))
        currency = request.POST.get('pg_currency', '0')

        try:
            order = PaymentOrder.objects.get(pk=order_id, external_order_id=external_order_id)

        except PaymentOrder.DoesNotExist as e:
            return self.return_error("Платеж не найден")

        if order.amount == amount and order.currency.code == currency and not order.has_end_status():
            return self.return_ok()

        return self.return_rejected()


class PayboxPaymentResultView(View, PayboxResponseMixin):
    def dispatch(self, request, *args, **kwargs):
        order_id = request.POST.get('pg_order_id', "")
        external_order_id = request.POST.get('pg_payment_id', "")
        result = int(request.POST.get('pg_result', '-1'))
        user_phone = request.POST.get('pg_user_phone', '')
        failure_code = request.POST.get('pg_failure_code', '0')
        failure_description = request.POST.get('pg_failure_description', '')

        try:
            order = PaymentOrder.objects.get(pk=order_id, external_order_id=external_order_id)

        except PaymentOrder.DoesNotExist as e:
            return self.return_error("Платеж не найден")

        if order.has_end_status():
            return self.return_error(f"Платеж уже находится в конечном статусе: {order.get_status_display()}")

        error = result != 1

        PaymentService.finalize_order(order, error, failure_code, failure_description, message=user_phone)

        return self.return_ok()
