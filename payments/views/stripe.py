import logging

import stripe
import stripe.error
from django.conf import settings
from django.contrib import messages
from django.db import transaction
from django.http import HttpResponse
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.views import View

from core.choices import PaymentSystems
from payments.models import PaymentOrder
from payments.services import PaymentService

logger = logging.getLogger(__name__)


class StripeWebhookView(View):
    @transaction.atomic()
    def dispatch(self, request, *args, **kwargs):
        payload = request.body
        signature = request.headers.get('stripe-signature')

        try:
            event = stripe.Webhook.construct_event(payload, signature, settings.STRIPE_WEBHOOK_SECRET)

            status = event.type
            payment_id = event.data.object.id

            try:
                order = PaymentOrder.objects.filter(external_order_id=payment_id, payment_system=PaymentSystems.STRIPE).first()

                if order:
                    PaymentService.update_payment_status(order, status)

            except AttributeError:
                logger.error(
                    f'Stripe Webhook error: Could not get payment_id or payment_status. '
                    f'Event: {event.to_dict_recursive()}'
                )

            except stripe.error.StripeError as e:
                logger.error(f'Stripe Webhook error: {e}')

        except ValueError:
            logger.error('Stripe Webhook error: Could not parse event')

        except stripe.error.SignatureVerificationError as e:
            logger.error(f'Stripe Webhook error: {e}')

        return HttpResponse(status=200)


class StripePaymentResultView(View):
    @transaction.atomic
    def dispatch(self, request, *args, **kwargs):
        order_id = request.GET.get('order')
        status = request.GET.get('status')

        order = PaymentOrder.objects.get(pk=order_id)

        if not all((order_id, status)):
            messages.error(request, _('Внутренняя ошибка. Необходимы данные платежного поручения'))
            return redirect(reverse('home'))

        PaymentService.update_payment_status(order, status)
        return redirect(reverse('payment_status_view', kwargs={'pk': order.id}))
