def normalize_filter(value, type, multiple=False):
    if not value:
        return

    if multiple and isinstance(value, list):
        try:
            return [type(v) for v in value]
        except ValueError:
            return []

    try:
        return type(value)

    except ValueError:
        return


def get_int(value, multiple=False):
    return normalize_filter(value, int, multiple)


def get_str(value, multiple=False):
    return normalize_filter(value, str, multiple)
