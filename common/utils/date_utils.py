import datetime


def parse_date_type(date_str):
    date_formats = ["%d.%m.%Y", "%d-%m-%Y", "%Y-%m-%d", "%Y.%m.%d"]
    for date_format in date_formats:
        try:
            return datetime.datetime.strptime(date_str, date_format).date()
        except (ValueError, TypeError):
            pass
