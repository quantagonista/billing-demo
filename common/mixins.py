class UpdateContextMixin:
    def update_context(self, context):
        """
        Used to mutate context in place.
        In order to reduce boilerplate code in TemplateViews
        """

        return context

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        self.update_context(context)

        return context
