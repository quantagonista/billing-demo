from django.db import models
from django.db.models import DateTimeField

from django.utils.translation import gettext_lazy as _

# Used to reduce boilerplate for nullable model fields
NULLABLE = {'null': True, 'blank': True}


class TimestampedModel(models.Model):
    created_at = DateTimeField(verbose_name=_('Дата создания'), auto_now_add=True)
    updated_at = DateTimeField(verbose_name=_('Дата обновления'), auto_now=True)

    class Meta:
        abstract = True
