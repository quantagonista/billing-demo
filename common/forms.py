from django.forms import (
    BoundField, ChoiceField,
    ModelMultipleChoiceField,
    ModelForm as DjangoModelForm,
    Form as DjangoForm,
)
from django.utils.safestring import mark_safe


class LabeledBoundField(BoundField):
    def label_tag(self, contents=None, attrs=None, label_suffix=None):
        css_class = "required"
        if not self.field.required:
            css_class = ""
        return super().label_tag(contents=contents, attrs={"class": css_class}, label_suffix='')

    def help_text_tag(self, contents=None, attrs=None, label_suffix=None):
        return mark_safe(f"<div class='helptext'>{self.help_text}</div>")


class CrmModelForm(DjangoModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name, field in self.fields.items():
            self._bound_fields_cache[name] = LabeledBoundField(self, field, name)


class CrmForm(DjangoForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name, field in self.fields.items():
            self._bound_fields_cache[name] = LabeledBoundField(self, field, name)


class BulmaBoundField(BoundField):
    def label_tag(self, contents=None, attrs=None, label_suffix=None):
        css_class = "label"
        if not self.field.required:
            css_class += " optional"
        return super().label_tag(contents=contents, attrs={"class": css_class}, label_suffix=label_suffix)

    def css_classes(self, extra_classes=None):
        return super().css_classes("input")

    def as_widget(self, widget=None, attrs=None, only_initial=False):
        if isinstance(self.field, ChoiceField):
            as_widget = super().as_widget(widget, attrs, only_initial)
            if isinstance(self.field, ModelMultipleChoiceField):
                return mark_safe('<div class="control"><div class="select is-multiple">' + as_widget + '</div></div>')
            return mark_safe('<div class="control"><div class="select">' + as_widget + '</div></div>')
        as_widget = super().as_widget(widget, {"class": "input"}, only_initial)
        return mark_safe('<div class="control">' + as_widget + '</div>')


class BulmaModelForm(CrmModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name, field in self.fields.items():
            self._bound_fields_cache[name] = BulmaBoundField(self, field, name)

    def as_div(self):
        return self._html_output(
            normal_row='<div class="field"%(html_class_attr)s>%(label)s %(field)s %(help_text)s <div class="help is-danger">%(errors)s</div></div>',
            error_row='<div class="help is-danger">%s</div>',
            row_ender='</div>',
            help_text_html='<p class="help">%s</p>',
            errors_on_separate_row=False, )
