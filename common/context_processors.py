from django.conf import settings


def date_formats(request):
    js_date_format = settings.DATE_FORMATS["JS_DATE"][request.LANGUAGE_CODE]
    js_date_time_format = settings.DATE_FORMATS["JS_DATE_TIME"][request.LANGUAGE_CODE]
    js_picker_date_format = settings.DATE_FORMATS["JS_PICKER_DATE"][request.LANGUAGE_CODE]

    return {
        'JS_DATE_FORMAT': js_date_format,
        'JS_DATE_TIME_FORMAT': js_date_time_format,
        'JS_PICKER_DATE_FORMAT': js_picker_date_format,
    }
