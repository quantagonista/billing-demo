from django.contrib.auth.decorators import login_required, user_passes_test
from django.utils.decorators import method_decorator, classonlymethod
from django.views import View
from django.views.generic import TemplateView, FormView

from common.mixins import UpdateContextMixin


@method_decorator(login_required, name='dispatch')
class LoginRequiredView(View):
    """ It used to protect unauthorized access """
    pass


class SuperUserRequiredView(LoginRequiredView):
    """ It used to allow access only to superusers """
    @classonlymethod
    def as_view(cls, **initkwargs):
        view = super().as_view(**initkwargs)

        decorator = user_passes_test(
            lambda user: user.is_superuser, login_url="/"
        )

        return decorator(view)


class CustomTemplateView(LoginRequiredView, UpdateContextMixin, TemplateView):
    pass


class CustomFormView(LoginRequiredView, UpdateContextMixin, FormView):
    pass


class SuperUserTemplateView(SuperUserRequiredView, UpdateContextMixin, TemplateView):
    pass


class SuperUserFormView(SuperUserRequiredView, UpdateContextMixin, FormView):
    pass
