from django.urls import path

from . import views
from .views.dashboard import DashboardView

urlpatterns = [
    path('dashboard/', DashboardView.as_view(), name='dashboard'),

    path('customers/', views.CustomerListView.as_view(), name='customer-list'),
    path('customers/fast-create/', views.CustomerFastCreateView.as_view(), name='customer-fast-create'),
    path('customers/create/', views.CustomerCreateView.as_view(), name='customer-create'),
    path('customers/<pk>/', views.CustomerDetailView.as_view(), name='customer-detail'),
    path('customers/<pk>/set-status/', views.CustomerUpdateStatusView.as_view(), name='customer-update-status'),
    path('customers/<pk>/edit/', views.CustomerUpdateView.as_view(), name='customer-update'),
    path('customers/<pk>/integration/', views.CustomerCreateExternalIntegrationView.as_view(), name='customer-create-integration'),
    path('customers/<pk>/activate/', views.CustomerActivateFormView.as_view(), name='customer-activate'),
    path('customers/<pk>/lock/', views.CustomerLockAccessView.as_view(), name='customer-lock-crm'),
    path('customers/<pk>/delete-crm/', views.CustomerDeleteCrmView.as_view(), name='customer-delete-crm'),

    path('archived-customers/', views.ArchivedCustomerListView.as_view(), name='archived-customer-list'),
    path('archived-customers/<pk>/delete-customer/', views.DeleteCustomerView.as_view(), name='delete-customer'),

    path('invoices/', views.InvoiceListView.as_view(), name='invoice-list'),
    path('invoices/<pk>/', views.InvoiceDetailView.as_view(), name='invoice-detail'),
    path('invoices/<pk>/pay/', views.InvoicePayView.as_view(), name='invoice-pay'),
    path('invoices/<int:invoice_pk>/items/create', views.InvoiceItemCreateView.as_view(), name='invoice-item-create'),
    path('invoices/<int:invoice_pk>/items/<int:pk>/edit', views.InvoiceItemUpdateView.as_view(), name='invoice-item-update'),

    path('licenses/', views.LicenseListView.as_view(), name='license-list'),
    path('customer/<pk>/licenses/create/', views.LicenseCreateView.as_view(), name='license-create'),
    path('customer/<pk>/invoices/create/', views.CustomerInvoiceCreateView.as_view(), name='invoice-create'),
    path('licenses/<pk>/', views.LicenseDetailView.as_view(), name='license-detail'),
    path('licenses/<pk>/edit/', views.LicenseUpdateView.as_view(), name='license-update'),
    path('licenses/<pk>/prolong/', views.LicenseProlongView.as_view(), name='license-prolong'),

    path('transactions/', views.TransactionListView.as_view(), name='transaction-list'),
    path('transactions/<pk>/cancel/', views.TransactionCancelView.as_view(), name='transaction-cancel'),
]
