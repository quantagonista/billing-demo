from . import constants as const


class BaseError(BaseException):
    default_message = const.SOMETHING_WENT_WRONG

    def __init__(self, message: str = None, *args):
        self._message = message
        super().__init__(*args)

    def __str__(self):
        return self.message

    @property
    def message(self):
        return self._message if self._message else self.default_message


class LicensePeriodIntersectionError(BaseError):
    default_message = const.LICENSE_PERIOD_INTERSECTION_ERROR


class InvalidDiscountAmountError(BaseError):
    default_message = const.INVALID_DISCOUNT_AMOUNT_ERROR


class InvalidLicenseDateError(BaseError):
    default_message = const.INVALID_LICENSE_DATE_ERROR


class InvalidCreditTransactionAmountError(BaseError):
    default_message = const.INVALID_CREDIT_TRANSACTION_AMOUNT_ERROR


class InvalidDebitTransactionError(BaseError):
    default_message = const.INVALID_DEBIT_TRANSACTION


class CRMIntegrationError(BaseError):
    default_message = const.CRM_INTEGRATION_BASE_ERROR


class InitPaymentError(BaseError):
    default_message = const.INIT_PAYMENT_ERROR
