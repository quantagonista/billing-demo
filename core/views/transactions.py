from django.shortcuts import redirect
from django.urls import reverse

from common.utils.date_utils import parse_date_type
from common.utils.parsing import get_int
from common.views import SuperUserTemplateView
from core import models
from core.services import TransactionService


class TransactionListView(SuperUserTemplateView):
    template_name = 'core/transactions/list.html'

    def update_context(self, context):
        filters = self.get_filters()
        context['transactions'] = TransactionService.filter(**filters).exclude(customer__status='archived')
        context['customers'] = models.Customer.objects.all()
        context['types'] = models.Transaction.TRANSACTION_TYPES
        context['filters'] = filters
        context['has_filters'] = any(filters.values())

    def get_filters(self):
        return {
            'customer': get_int(self.request.GET.get('customer')),
            'type': get_int(self.request.GET.get('type')),
            'start_date': parse_date_type(self.request.GET.get('start_date')),
            'end_date': parse_date_type(self.request.GET.get('end_date')),
        }


class TransactionCancelView(SuperUserTemplateView):
    def post(self, request, *args, **kwargs):
        transactions = models.Transaction.objects.filter(pk=self.kwargs.get('pk'))
        TransactionService.cancel_transactions(transactions)

        return redirect(reverse('transaction-list'))
