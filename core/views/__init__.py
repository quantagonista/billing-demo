from .main import not_found_page
from .customers import (
    CustomerListView, ArchivedCustomerListView, DeleteCustomerView,
    CustomerCreateView, CustomerFastCreateView, CustomerDetailView,
    CustomerUpdateView, CustomerCreateExternalIntegrationView,
    CustomerUpdateStatusView, CustomerLockAccessView,
    CustomerDeleteCrmView, CustomerActivateFormView
)
from .invoices import (
    InvoiceListView, InvoiceDetailView, InvoiceItemCreateView,
    InvoiceItemUpdateView, InvoicePayView, CustomerInvoiceCreateView
)
from .licenses import (
    LicenseListView, LicenseCreateView, LicenseDetailView,
    LicenseUpdateView, LicenseProlongView
)
from .transactions import TransactionListView, TransactionCancelView
