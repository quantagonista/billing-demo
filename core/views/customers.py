from django.contrib import messages
from django.db import transaction
from django.shortcuts import redirect, get_object_or_404
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from common.utils.parsing import get_int, get_str
from common.views import SuperUserTemplateView, SuperUserFormView, SuperUserRequiredView
from core import services, forms
from core.choices import LicenseTypes
from core.exceptions import CRMIntegrationError
from core.forms.customer_forms import CustomerActivateForm
from core.models import Customer, Transaction
from core.models.choices import CustomerStatuses, CrmStatuses
from core.services import CustomerService, CrmIntegrationService
from core.services.license import LicenseCheckService, LicenseService


class CustomerListView(SuperUserTemplateView):
    template_name = 'core/customers/list.html'
    customer_statuses = Customer.objects.only('status')

    def update_context(self, context):
        filters = self.get_filters()
        context['filters'] = filters
        context['params'] = self.request.GET
        context['statuses'] = CustomerStatuses
        context['types'] = services.CustomerService.get_customer_types_as_choices()
        context['customers'] = services.CustomerService.get_filtered_customers(**filters).exclude(status='archived')
        context['active_customers'] = self.customer_statuses.filter(status='active').count()
        context['inactive_customers'] = self.customer_statuses.filter(status='inactive').count()
        context['test_customers'] = self.customer_statuses.filter(status='test_drive').count()

    def get_filters(self):
        return {
            'types': get_int(self.request.GET.getlist('types'), multiple=True),
            'ids': get_int(self.request.GET.getlist('id'), multiple=True),
            'statuses': get_str(self.request.GET.getlist('statuses'), multiple=True),
            'search': self.request.GET.get('search'),
        }


class ArchivedCustomerListView(SuperUserTemplateView):
    template_name = 'core/customers/archived-list.html'

    def update_context(self, context):
        context['customers'] = Customer.objects.filter(status='archived')


class DeleteCustomerView(SuperUserRequiredView):
    def post(self, request, **kwargs):
        customer = Customer.objects.get(id=kwargs['pk'])
        CustomerService.delete_customer(customer)
        messages.success(request, _("Данные успешно удалены"))
        return redirect('archived-customer-list')


class CustomerDetailView(SuperUserTemplateView):
    template_name = 'core/customers/detail.html'

    def update_context(self, context):
        customer = get_object_or_404(services.CustomerService.model, pk=self.kwargs.get('pk'))

        context['customer'] = customer
        context['statuses'] = CustomerStatuses
        context['crm_integration'] = customer.get_crm_integration()
        context['licenses'] = customer.licenses.all()
        context['invoices'] = customer.invoices.all()

        last_transaction = customer.transactions.filter(type=Transaction.DEBIT).order_by("-created_at").first()
        context['last_transaction'] = last_transaction
        context['license_end_date'] = LicenseCheckService.get_customer_license_end_date(customer)
        context['today'] = timezone.now().date()


class CustomerUpdateStatusView(SuperUserRequiredView):
    def post(self, request, *args, **kwargs):
        customer = get_object_or_404(services.CustomerService.model, pk=self.kwargs.get('pk'))
        status = request.POST.get('status')
        if status and status != customer.status:
            customer.status = status
            customer.save()
            messages.success(request, _('Статус успешно изменен на: {status}').format(status=customer.get_status_display()))
        return redirect(reverse('customer-detail', kwargs={'pk': customer.pk}))


class CustomerFastCreateView(SuperUserFormView):
    template_name = 'core/customers/fast-create.html'
    form_class = forms.CustomerFastCreateForm

    def form_valid(self, form):
        try:
            invoice = self.handle_creation(form)
            return redirect(reverse('invoice-detail', kwargs={'pk': invoice.pk}))

        except CRMIntegrationError as e:
            messages.error(self.request, e.message)
            return self.form_invalid(form)

    def handle_creation(self, form):
        customer_data = {
            'name': form.cleaned_data.get('name'),
            'type': form.cleaned_data.get('type'),
            'url': form.cleaned_data.get('url'),
            'contact_phone_number': form.cleaned_data.get('contact_phone_number'),
            'contact_person': form.cleaned_data.get('contact_person'),
            'contact_skype': form.cleaned_data.get('contact_skype'),
            'contact_email': form.cleaned_data.get('contact_email'),
            'created_by': self.request.user,
            'users_count': form.cleaned_data.get('users_count'),
            'crm_status': CrmStatuses.ACTIVE,
        }

        license_data = {
            'license_entity': form.cleaned_data.get('license_entity'),
            'start_date': form.cleaned_data.get('license_start_date'),
            'price': form.cleaned_data.get('license_price'),
            'type': LicenseTypes.MONTHLY,
            'first_payment': form.cleaned_data.get('first_payment'),
            'first_payment_months': form.cleaned_data.get('first_payment_months'),
        }

        with transaction.atomic():
            customer = CustomerService.create_customer(**customer_data)
            __, invoice = LicenseService.create_license_with_first_invoice(customer, **license_data)
            crm_server = form.cleaned_data.get('crm_server')
            CrmIntegrationService.create_and_integrate_crm(customer, crm_server)

        messages.success(self.request, _('Клиент успешно создан'))
        messages.success(self.request, _('Интеграция успешно проведена'))
        messages.success(self.request, _('Приглашение отправлено: {email}').format(email=customer.contact_email))

        return invoice


class CustomerCreateView(SuperUserFormView):
    template_name = 'core/customers/create.html'
    form_class = forms.CustomerCreateForm

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.users_count = 0
        form.save()

        customer = self.request.GET.get('customer')
        if customer:
            return redirect(reverse('customer-detail', kwargs={'pk': customer}))
        return redirect(reverse('customer-list'))


class CustomerUpdateView(CustomerFastCreateView):
    template_name = 'core/customers/edit.html'
    form_class = forms.CustomerUpdateForm

    def form_valid(self, form):
        form.save()
        return redirect(reverse('customer-detail', kwargs={"pk": form.instance.id}))

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['instance'] = get_object_or_404(Customer, pk=self.kwargs.get('pk'))

        return kwargs


class CustomerCreateExternalIntegrationView(SuperUserFormView):
    template_name = "core/customers/crm_integration.html"
    form_class = forms.CustomerExternalIntegrationForm

    def get_object(self):
        return get_object_or_404(Customer, pk=self.kwargs.get('pk'))

    def get_context_data(self, **kwargs):
        return {
            'customer': self.get_object(),
            **super(CustomerCreateExternalIntegrationView, self).get_context_data(**kwargs),
        }

    def form_valid(self, form):
        customer = self.get_object()

        if customer.has_crm_integration():
            messages.error(self.request, _('Интеграция уже проведена'))
            return self.form_invalid(form)

        server = form.cleaned_data.get('crm_server')

        try:
            CrmIntegrationService.create_crm_integration(customer, server)
            messages.success(self.request, _('Интеграция успешно проведена'))
            return redirect(reverse('customer-detail', kwargs={"pk": customer.id}))

        except CRMIntegrationError as e:
            messages.error(self.request, e.message)
            return self.form_invalid(form)


class CustomerLockAccessView(SuperUserRequiredView):
    def post(self, request, *args, **kwargs):
        customer = get_object_or_404(Customer, pk=self.kwargs.get('pk'))

        try:
            CrmIntegrationService.lock_crm_access_for(customer)
            customer.crm_status = CrmStatuses.LOCKED
            customer.save(update_fields=['crm_status'])
            messages.success(self.request, _('Доступ заблокирован'))

        except CRMIntegrationError as e:
            messages.error(self.request, e.message)

        return redirect(reverse('customer-detail', kwargs={"pk": customer.id}))


class CustomerActivateFormView(SuperUserFormView):
    form_class = CustomerActivateForm
    template_name = 'core/customers/activate.html'

    def dispatch(self, request, *args, **kwargs):
        self.customer = get_object_or_404(Customer, pk=self.kwargs.get('pk'))
        return super().dispatch(request, *args, **kwargs)

    def update_context(self, context):
        context['customer'] = self.customer

    def get_form_kwargs(self):
        return {
            'customer': self.customer,
            **super().get_form_kwargs(),
        }

    def form_valid(self, form):
        with transaction.atomic():
            self.customer.status = CustomerStatuses.ACTIVE
            self.customer.save()
            LicenseService.disable_old_licenses(self.customer)
            LicenseService.create_license_with_first_invoice(customer=self.customer, **form.cleaned_data)
            LicenseCheckService.check_license_and_update_crm_for(self.customer)

        return redirect(reverse('customer-detail', kwargs={"pk": self.customer.id}))


class CustomerDeleteCrmView(SuperUserRequiredView):
    def post(self, request, *args, **kwargs):
        customer = get_object_or_404(Customer, pk=self.kwargs.get('pk'))

        try:
            CrmIntegrationService.delete_crm(customer)
            messages.success(self.request, _('Данные CRM успешно удалены'))

        except CRMIntegrationError as e:
            messages.error(self.request, e.message)

        return redirect(reverse('customer-detail', kwargs={"pk": customer.id}))
