from django.utils import timezone

from common.views import SuperUserTemplateView
from core.services.dashboard import DashboardService
from core.services.mrr import MRRService


class DashboardView(SuperUserTemplateView):
    template_name = 'core/dashboard.html'

    def get_context_data(self, **kwargs):
        dashboard_info = DashboardService.get_dashboard_info()
        transactions_by_months = MRRService.get_transactions_by_months()
        paid_for_months = MRRService.get_paid_for_months()

        return {
            'transactions_by_months': transactions_by_months,
            'paid_for_months': paid_for_months,
            'test_drive_customers': dashboard_info.get('test_drive_customers'),
            'paid_customers': dashboard_info.get('paid_customers'),
            'need_to_pay_customers':  dashboard_info.get('need_to_pay_customers'),
            'late_customers': dashboard_info.get('late_customers'),
            'inactive_customers': dashboard_info.get('inactive_customers'),
            **super().get_context_data(**kwargs)
        }


class MRRView(SuperUserTemplateView):
    template_name = 'core/mrr.html'

    def get_context_data(self, **kwargs):
        customers, paid_for_months = MRRService.get_monthly_payment_by_customers()
        current_month = timezone.now().date().replace(day=1)

        return {
            'customers': customers,
            'paid_for_months': paid_for_months,
            'current_month': current_month
        }