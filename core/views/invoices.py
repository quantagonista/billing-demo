from django.contrib import messages
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from common.utils.date_utils import parse_date_type
from common.utils.parsing import get_int
from common.views import SuperUserTemplateView, SuperUserFormView
from core import models, forms, services
from core.exceptions import BaseError


class InvoiceListView(SuperUserTemplateView):
    template_name = 'core/invoices/list.html'

    def update_context(self, context):
        filters = self.get_filters()

        context['filters'] = filters
        context['has_filters'] = any(filters.values())
        context['customers'] = models.Customer.objects.only('id', 'name')
        context['invoices'] = (
            models.Invoice.objects
            .get_filtered_invoices(**filters)
            .exclude(customer__status='archived')
        )

    def get_filters(self):
        return {
            'customer': get_int(self.request.GET.get('customer')),
            'search': self.request.GET.get('search'),
            'start_date': parse_date_type(self.request.GET.get('start_date')),
            'end_date': parse_date_type(self.request.GET.get('end_date')),
        }


class CustomerInvoiceCreateView(SuperUserFormView):
    template_name = 'core/invoices/create.html'
    form_class = forms.InvoiceCreateForm

    def dispatch(self, request, *args, **kwargs):
        self.customer = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def get_object(self):
        return get_object_or_404(models.Customer, pk=self.kwargs["pk"])

    def get_form_kwargs(self):
        return {
            'customer': self.customer,
            **super().get_form_kwargs()
        }

    def get_context_data(self, **kwargs):
        return {
            'customer': self.customer,
            **super().get_context_data(**kwargs)
        }

    def form_valid(self, form):
        services.InvoiceService.create_invoice(**form.cleaned_data)
        return redirect(
            reverse('customer-detail', kwargs={'pk': self.customer.id})
        )


class InvoiceDetailView(SuperUserTemplateView):
    template_name = 'core/invoices/detail.html'

    def update_context(self, context):
        invoice = get_object_or_404(models.Invoice, pk=self.kwargs.get('pk'))

        context['invoice'] = invoice
        context['invoice_items'] = invoice.items.all()
        context['pay_url'] = self.request.build_absolute_uri(
            reverse('cabinet:invoice-pay', kwargs={'slug': invoice.slug})
        )


class InvoiceItemCreateView(SuperUserFormView):
    template_name = 'core/invoices/item-update.html'
    form_class = forms.InvoiceItemForm
    invoice: models.Invoice = None

    def dispatch(self, request, *args, **kwargs):
        self.invoice = get_object_or_404(
            models.Invoice, pk=self.kwargs.get('invoice_pk')
        )

        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        return {
            'invoice': self.invoice,
            **super().get_context_data(**kwargs),
        }

    def get_form_class(self):
        return {
            'invoice_item': forms.InvoiceItemForm,
            'license_invoice_item': forms.LicenceInvoiceItemForm,
        }.get(self.request.GET.get('type'), forms.InvoiceItemForm)

    def get_form_kwargs(self):
        return {'invoice': self.invoice, **super().get_form_kwargs()}

    def form_valid(self, form):
        form.instance.invoice = self.invoice
        form.save()
        return redirect(reverse('invoice-detail', kwargs={'pk': self.invoice.id}))


class InvoiceItemUpdateView(InvoiceItemCreateView):
    def get_form_kwargs(self):
        instance = get_object_or_404(
            models.InvoiceItem,
            pk=self.kwargs.get('pk'),
            invoice=self.invoice
        ).get_real_instance()
        return {'instance': instance, **super().get_form_kwargs()}


class InvoicePayView(SuperUserFormView):
    template_name = 'core/invoices/pay.html'
    form_class = forms.InvoicePayForm
    invoice: models.Invoice = None

    def dispatch(self, request, *args, **kwargs):
        self.invoice = get_object_or_404(models.Invoice, pk=self.kwargs.get('pk'))
        return super().dispatch(request, *args, **kwargs)

    def update_context(self, context):
        context['invoice'] = self.invoice
        context['customer_balance'] = models.Transaction.objects.get_customer_balance(self.invoice.customer)

    def get_initial(self):
        return {
            'invoice': self.invoice,
            **super().get_initial()
        }

    def form_valid(self, form):
        payment_method = form.cleaned_data.get('payment_method')
        amount = form.cleaned_data.get('amount')
        payment_date = form.cleaned_data.get('payment_date')

        payment_date = timezone.now().replace(payment_date.year, payment_date.month, payment_date.day)

        try:
            has_paid = services.InvoiceService.pay_invoice_manually(self.invoice, payment_method, amount, payment_date)
            if has_paid:
                messages.success(self.request, _('Счет успешно оплачен'))
            else:
                messages.warning(self.request, _('Средства поступили на счет клиента, однако сумма недостаточна для оплаты счета'))

        except BaseError as e:
            messages.error(self.request, e.message)
            return self.form_invalid(form)

        if self.invoice.customer:
            services.LicenseCheckService.check_license_and_update_crm_for(self.invoice.customer)

        return self.redirect()

    def redirect(self):
        license_id = self.request.GET.get('license')
        if license_id:
            return redirect(reverse('license-detail', kwargs={'pk': license_id}))

        return redirect(reverse('invoice-list'))
