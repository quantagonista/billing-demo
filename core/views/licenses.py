from django.contrib import messages
from django.shortcuts import redirect, get_object_or_404
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from common.utils.date_utils import parse_date_type
from common.utils.parsing import get_int
from common.views import SuperUserTemplateView, SuperUserFormView, SuperUserRequiredView
from core import models, forms, exceptions
from core.choices import LicenseTypes
from core.services import InvoiceService
from core.services.license import LicenseCheckService


class LicenseListView(SuperUserTemplateView):
    template_name = 'core/licenses/list.html'

    def update_context(self, context):
        filters = self.get_filters()

        context['filters'] = filters
        context['has_filters'] = any(filters.values())

        context['customers'] = models.Customer.objects.only('id', 'name')
        context['license_entities'] = models.LicenseEntity.objects.only('id', 'name')
        context['licenses'] = models.License.objects.get_filtered_licenses(**filters).exclude(customer__status='archived')

    def get_filters(self):
        return {
            'license_entity': get_int(self.request.GET.get('license_entity')),
            'customer': get_int(self.request.GET.get('customer')),
            'start_date': parse_date_type(self.request.GET.get('start_date')),
            'end_date': parse_date_type(self.request.GET.get('end_date')),
        }


class LicenseDetailView(SuperUserTemplateView):
    template_name = 'core/licenses/detail.html'

    def update_context(self, context):
        license = get_object_or_404(models.License, pk=self.kwargs.get('pk'))
        invoice_ids = set(models.LicenseInvoiceItem.objects.filter(license=license).values_list('invoice_id', flat=True))

        context['license'] = license
        context['invoices'] = models.Invoice.objects.filter(pk__in=invoice_ids)


class LicenseCreateView(SuperUserFormView):
    template_name = 'core/licenses/create.html'
    form_class = forms.LicenseCreateForm

    def get_object(self):
        customer_id = self.kwargs["pk"]
        return get_object_or_404(models.Customer, pk=customer_id)

    def get_initial(self):
        start_date = timezone.now()
        end_date = start_date + timezone.timedelta(days=6)

        return {
            'customer': self.get_object(),
            'license_entity': models.LicenseEntity.objects.first(),
            'type': LicenseTypes.MONTHLY,
            'start_date': start_date,
            'end_date': end_date,
            **super().get_initial(),
        }

    def get_context_data(self, **kwargs):
        return {
            'customer': self.get_object(),
            **super().get_context_data(**kwargs)
        }

    def form_valid(self, form):
        form.save()
        LicenseCheckService.check_license_and_update_crm_for(form.instance.customer)
        return self.redirect()

    def redirect(self):
        customer_id = self.request.GET.get('customer')
        if customer_id:
            return redirect(reverse('customer-detail', kwargs={'pk': customer_id}))

        return redirect(reverse('license-list'))


class LicenseUpdateView(SuperUserFormView):
    template_name = 'core/licenses/create.html'
    form_class = forms.LicenseCreateForm

    def get_context_data(self, **kwargs):
        license_ = self.get_object()

        return {
            'customer': license_.customer,
            **super().get_context_data(**kwargs)
        }

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['instance'] = self.get_object()

        return kwargs

    def form_valid(self, form):
        form.save()
        LicenseCheckService.check_license_and_update_crm_for(form.instance.customer)
        return self.redirect()

    def redirect(self):
        license_id = self.request.GET.get('license')
        if license_id:
            return redirect(reverse('license-detail', kwargs={'pk': license_id}))

        customer_id = self.request.GET.get('customer')
        if customer_id:
            return redirect(reverse('customer-detail', kwargs={'pk': customer_id}))

        return redirect(reverse('license-list'))

    def get_object(self):
        return get_object_or_404(models.License, pk=self.kwargs.get('pk'))


class LicenseProlongView(SuperUserRequiredView):
    def post(self, request, *args, **kwargs):
        license = get_object_or_404(models.License, pk=self.kwargs.get('pk'))
        try:
            invoice = InvoiceService.create_new_for_license(license)
            LicenseCheckService.check_license_and_update_crm_for(license.customer)
            messages.success(request, _('Лицензия успешно продлена'))
            return redirect(reverse('invoice-detail', kwargs={'pk': invoice.id}))

        except exceptions.LicensePeriodIntersectionError as e:
            messages.error(request, e.message)
            return redirect(reverse('license-detail', kwargs={'pk': license.id}))
