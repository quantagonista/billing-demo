from django.shortcuts import render, redirect
from django.urls import reverse

from common.views import CustomTemplateView


def not_found_page(request, exception):
    return render(request, "404.html")


class HomeView(CustomTemplateView):
    template_name = "cabinet/home.html"

    def update_context(self, context):
        customer = self.request.user.get_customer()

        context['customer'] = customer
        context['current_license'] = customer.get_current_license()

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_superuser:
            return redirect(reverse('dashboard'))
        return super().dispatch(request, *args, **kwargs)