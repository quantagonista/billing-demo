from django.db.models import TextChoices
from django.utils.translation import gettext_lazy as _


class LicenseTypes(TextChoices):
    TESTING = 'test', _('Тестовый период')
    MONTHLY = 'monthly', _('Месячная')
    ANNUAL = 'annual', _('Годовая')


class PaymentSystems(TextChoices):
    PAYBOX = 'paybox', "Paybox"
    STRIPE = 'stripe', "Stripe"
    COMMON = 'common_api', _("Общий платежный шлюз")


class PaymentOrderStatuses(TextChoices):
    NEW = 'new', _("Новый")
    PROCESSING = 'in_processing', _("В обработке")
    REVOKED = 'revoked', _("Отозван")
    SUCCESS = 'success', _("Успешный")
    ERROR = 'error', _("Ошибка")
    CANCELED = 'canceled', _("Отменен")
