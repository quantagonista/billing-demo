from rest_framework.serializers import ModelSerializer

from core.models import Invoice


class InvoiceSerializer(ModelSerializer):
    class Meta:
        model = Invoice
        fields = ["id", "title", "final_amount"]