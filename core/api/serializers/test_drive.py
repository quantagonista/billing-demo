from rest_framework.exceptions import ValidationError
from rest_framework.fields import CharField, EmailField
from rest_framework.serializers import Serializer

from core.constants import INVALID_CUSTOMER_ORG_TYPE, CUSTOMER_BY_EMAIL_EXISTS, CUSTOMER_BY_NAME_EXISTS
from core.services import CustomerService


class TestDriveCustomerCreateSerializer(Serializer):
    name = CharField()
    type = CharField()
    currency = CharField()
    language = CharField()

    full_name = CharField()
    email = EmailField()
    phone = CharField()

    def validate_email(self, email):
        if CustomerService.model.objects.filter(contact_email=email).exists():
            raise ValidationError(CUSTOMER_BY_EMAIL_EXISTS)
        return email

    def validate_name(self, name):
        if CustomerService.model.objects.filter(name=name).exists():
            raise ValidationError(CUSTOMER_BY_NAME_EXISTS)
        return name

    def validate_type(self, customer_type):
        for type_ in CustomerService.get_customer_types_as_choices():
            if type_.code == customer_type:
                return type_

        raise ValidationError(INVALID_CUSTOMER_ORG_TYPE)
