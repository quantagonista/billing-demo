from rest_framework.response import Response
from rest_framework.views import APIView

from core.api.auth.token import ExternalCrmTokenAuthentication
from core.constants import (
    INVALID_CUSTOMER_BY_TOKEN, INVALID_CUSTOMER_ORG_ID,
    CRM_STATUS_CHANGE_SUCCESS,CRM_INVALID_STATUS
)
from core.models.choices import CrmStatuses


class CustomerCrmStatusUpdateAPIView(APIView):
    authentication_classes = [ExternalCrmTokenAuthentication]
    permission_classes = []

    def post(self, request, *args, **kwargs):
        org_id = request.data.get('org_id')
        status = request.data.get('status')
        customer = request.user.customer

        if status not in CrmStatuses:
            return Response({'message': CRM_INVALID_STATUS}, status=400)

        if not customer:
            return Response({'message': INVALID_CUSTOMER_BY_TOKEN}, status=400)

        if not org_id == customer.org_id:
            return Response({'message': INVALID_CUSTOMER_ORG_ID}, status=400)

        customer.crm_status = status
        customer.save(update_fields=['crm_status'])
        return Response({'message': CRM_STATUS_CHANGE_SUCCESS}, status=200)
