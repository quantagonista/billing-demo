from rest_framework.response import Response
from rest_framework.views import APIView

from core.api.permissions.auth import ExternalTokenAuthenticationPermission
from core.api.serializers.test_drive import TestDriveCustomerCreateSerializer
from core.constants import INVALID_CUSTOMER_DATA, CUSTOMER_CREATE_SUCCESS
from core.exceptions import BaseError
from core.services.test_drive import TestDriveFacadeService


class CreateTestDriveCustomerAPIView(APIView):
    permission_classes = [ExternalTokenAuthenticationPermission]
    authentication_classes = []

    def post(self, request, *args, **kwargs):
        serializer = TestDriveCustomerCreateSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(
                status=400,
                data={
                    'message': INVALID_CUSTOMER_DATA,
                    'errors': serializer.errors
                }
            )

        try:
            accept_url = TestDriveFacadeService.perform_test_drive_integration(**serializer.validated_data)
            return Response(
                status=200,
                data={
                    'message': CUSTOMER_CREATE_SUCCESS,
                    'accept_url': accept_url
                }
            )

        except BaseError as e:
            return Response(status=400, data={'message': e.message})
