from rest_framework.authentication import BaseAuthentication

from core.models import ExternalCrmIntegration


class ExternalCrmTokenAuthentication(BaseAuthentication):
    def authenticate(self, request):
        token = request.META.get("HTTP_AUTHORIZATION")
        if token:
            token_object = ExternalCrmIntegration.objects.filter(value=token).first()
            if token_object:
                return token_object.user, token
