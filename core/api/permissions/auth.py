from django.conf import settings
from rest_framework.permissions import BasePermission


class ExternalTokenAuthenticationPermission(BasePermission):
    def has_permission(self, request, view):
        return request.META.get("HTTP_AUTHORIZATION") == settings.EXTERNAL_AUTH_TOKEN
