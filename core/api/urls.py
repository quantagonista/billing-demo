from django.urls import path

from core.api.views.organization import CustomerCrmStatusUpdateAPIView
from core.api.views.test_drive import CreateTestDriveCustomerAPIView
from core.api.views.invoices import NewInvoicesAPIView

urlpatterns = [
    path('new_invoices/', NewInvoicesAPIView.as_view(), name='new_invoices'),
    path('test-drive/', CreateTestDriveCustomerAPIView.as_view(), name='test_drive_flow'),
    path('update-crm-status/', CustomerCrmStatusUpdateAPIView.as_view(), name='update_crm_status'),
]

