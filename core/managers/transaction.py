from django.db import models
from django.db.models import Sum, F, FloatField


class TransactionManager(models.Manager):
    def only_active(self):
        return self.filter(is_canceled=False)

    def get_customer_balance(self, customer):
        balance = (
            self.only_active()
            .filter(customer=customer)
            .aggregate(
                total=Sum(F('type') * F('amount'),
                output_field=FloatField())
            )
        )

        return balance['total'] or 0
