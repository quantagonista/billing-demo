from django.db import models


class InvoiceManager(models.Manager):
    def get_filtered_invoices(self, customer: int, search, start_date, end_date):
        invoices = self.filter()

        if customer:
            invoices = invoices.filter(customer_id=customer)

        if search:
            invoices = invoices.filter(title__icontains=search)

        if start_date:
            invoices = invoices.filter(payment_date__gte=start_date)

        if end_date:
            invoices = invoices.filter(payment_date__lte=end_date)

        return invoices
