import datetime

from django.db import models
from django.db.models import Q



class LicenseManager(models.Manager):
    def overlapping_licenses(self,
                             customer: "Customer",
                             license_entity: "LicenseEntity",
                             start_date: datetime.date,
                             end_date: datetime.date,
                             license_id: int = None):

        overlap_condition = Q(end_date__gte=start_date, start_date__lt=end_date)

        licenses = self.filter(
            overlap_condition, customer=customer, license_entity=license_entity
        )

        if license_id:
            licenses = licenses.exclude(pk=license_id)

        return licenses

    def get_filtered_licenses(self,
                              customer: "Customer" = None,
                              license_entity: "LicenseEntity" = None,
                              start_date: datetime.datetime = None,
                              end_date: datetime.datetime = None):

        licenses = self.filter()

        if customer:
            licenses = licenses.filter(customer_id=customer)

        if license_entity:
            licenses = licenses.filter(license_entity_id=license_entity)

        if start_date:
            licenses = licenses.filter(start_date__gte=start_date)

        if end_date:
            licenses = licenses.filter(end_date__lte=end_date)

        return licenses.order_by("-start_date")
