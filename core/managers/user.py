from django.contrib.auth.models import UserManager as DjangoUserManager

from .. import models


class CustomUserManager(DjangoUserManager):
    def create_superuser(self, username, email=None, password=None, **extra_fields):
        user = super().create_superuser(username, email, password, **extra_fields)
        self._create_customer(user)

    @staticmethod
    def _create_customer(user):
        customer_type, _ = models.CustomerType.objects.get_or_create(name='Внутренний')

        customer, _ = models.Customer.objects.get_or_create(
            type=customer_type,
            created_by=user,
            users_count=0,
            name='Внутренний клиент',
            description='Клиент созданный для внутренних целей'
        )

        user.customer = customer
        user.save()

        return customer
