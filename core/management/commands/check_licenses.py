import logging

from django.core.management import BaseCommand

from core.services.license import LicenseCheckService

logger = logging.getLogger("django")


class Command(BaseCommand):
    def handle(self, *args, **options):
        LicenseCheckService.check_all_licenses()
