from django.core.management import BaseCommand
from django.utils import timezone
import logging

from core.services import InvoiceService

logger = logging.getLogger("django")


class Command(BaseCommand):
    def handle(self, *args, **options):
        current_timestamp = timezone.now()

        logger.info(f"Invoices checking started at {current_timestamp}")
        created, updated = InvoiceService.generate_invoices_for_ending_licences(current_timestamp)

        logger.info(
            f"Invoices checking stopped at {timezone.now()}; "
            f"{created} invoices generated {updated} invoices updated"
        )
