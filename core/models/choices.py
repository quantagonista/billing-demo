from django.db import models
from django.utils.translation import gettext_lazy as _


class CustomerStatuses(models.TextChoices):
    TEST_DRIVE = 'test_drive', _('Тестовый период')
    INACTIVE = 'inactive', _('Неактивный')
    ACTIVE = 'active', _('Активный')
    ARCHIVED = 'archived', _('Архив')


class OrgTypesEnum(models.TextChoices):
    COURSES = "courses", _("Курсы")
    CORPORATE_COURSES = "corporate_courses", _("Корпоративные курсы")
    CRM = "crm", _("Универсальный CRM")


class CrmStatuses(models.TextChoices):
    UNKNOWN = 'unknown', _('Неизвестен')
    ACTIVE = 'active', _('Активно')
    LOCKED = 'locked', _('Заблокировано')
    DELETED = 'deleted', _('Удалено')
