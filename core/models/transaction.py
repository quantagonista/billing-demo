from django.db import models, transaction
from django.utils.translation import gettext_lazy as _

from common.models import TimestampedModel, NULLABLE
from . import Customer, Invoice
from .. import exceptions, managers, constants as const


class Transaction(TimestampedModel):
    DEBIT = 1
    CREDIT = -1

    TRANSACTION_TYPES = (
        (DEBIT, _('Пополнение')),
        (CREDIT, _('Снятие')),
    )

    objects = managers.TransactionManager()

    customer = models.ForeignKey(Customer, on_delete=models.PROTECT, verbose_name=_('Клиент'), related_name='transactions')
    payment_method = models.ForeignKey(to='PaymentMethod', on_delete=models.SET_NULL, verbose_name=_('Способ оплаты'), **NULLABLE)
    invoice = models.OneToOneField(Invoice, models.PROTECT, **NULLABLE, verbose_name=_('Счет на оплату'), related_name='transaction')

    type = models.IntegerField(verbose_name=_('Тип'), choices=TRANSACTION_TYPES)
    amount = models.DecimalField(verbose_name=_('Сумма $'), max_digits=15, decimal_places=2)
    is_canceled = models.BooleanField(verbose_name=_('Ануллирована?'), default=False)

    def __str__(self):
        return f"{self.created_at.strftime('%Y-%m-%d %H:%M')} - {self.get_type_display()}: {self.amount}"

    @transaction.atomic
    def save(self, **kwargs):
        if self.type == self.DEBIT and self.invoice:
            raise exceptions.InvalidDebitTransactionError(const.INVALID_INVOICE_FOR_DEBIT_TRANSACTION)

        if self.type == self.CREDIT:
            if not self.invoice:
                raise exceptions.InvalidCreditTransactionAmountError(message=const.INVALID_INVOICE_FOR_CREDIT_TRANSACTION)

            if self.amount != self.invoice.final_amount:
                raise exceptions.InvalidCreditTransactionAmountError(message=const.INVALID_CREDIT_TRANSACTION_FOR_INVOICE)

            if not self.is_canceled:
                total_amount = Transaction.objects.get_customer_balance(customer=self.customer)

                if self.amount > total_amount:
                    raise exceptions.InvalidCreditTransactionAmountError()

                self.invoice.mark_as_paid(self.created_at)

        super().save(**kwargs)

    class Meta:
        verbose_name = _('Транзакция')
        verbose_name_plural = _('Транзакции')
        default_related_name = 'transactions'


class PaymentMethod(models.Model):
    name = models.CharField(verbose_name=_('Название'), max_length=50)
    is_active = models.BooleanField(verbose_name=_('Активен?'), default=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Способ оплаты')
        verbose_name_plural = _('Способы оплаты')
