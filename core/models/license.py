from datetime import date

from ckeditor.fields import RichTextField
from dateutil.relativedelta import relativedelta
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from common.models import TimestampedModel, NULLABLE
from . import Customer
from .. import exceptions, managers
from ..choices import LicenseTypes


class License(TimestampedModel):
    objects = managers.LicenseManager()

    customer = models.ForeignKey(to=Customer, on_delete=models.PROTECT, verbose_name=_('Клиент'), related_name="licenses")
    license_entity = models.ForeignKey(to='LicenseEntity', on_delete=models.PROTECT, verbose_name=_('Лицензия за'))

    type = models.CharField(verbose_name=_('Тип'), choices=LicenseTypes.choices, max_length=50)
    start_date = models.DateField(verbose_name=_('Дата начала'))
    end_date = models.DateField(verbose_name=_('Дата окончания'))
    price = models.DecimalField(verbose_name=_('Цена $'), max_digits=10, decimal_places=2)
    discount_percentage = models.PositiveSmallIntegerField(verbose_name=_('Скидка %'), default=0)
    description = RichTextField(verbose_name=_('Описание'), **NULLABLE)
    active = models.BooleanField(verbose_name=_("Активен"), default=True)

    @property
    def title(self):
        return self.license_entity.name

    @property
    def days_left(self):
        return (self.end_date - timezone.now().date()).days

    @property
    def length(self):
        if self.type == LicenseTypes.MONTHLY:
            return relativedelta(months=1)

        elif self.type == LicenseTypes.ANNUAL:
            return relativedelta(years=1)

        elif self.type == LicenseTypes.TESTING:
            return relativedelta(days=14)

    def __str__(self):
        return f'{self.customer} - {self.license_entity}: {self.start_date} - {self.end_date}'

    def save(self, **kwargs):
        if self.does_overlap_other():
            raise exceptions.LicensePeriodIntersectionError()

        return super().save(**kwargs)

    def does_overlap_other(self):
        return License.objects.overlapping_licenses(self.customer, self.license_entity, self.start_date, self.end_date, self.pk).exists()

    def prolong_license(self, new_date: date):
        if new_date < self.end_date:
            raise exceptions.InvalidLicenseDateError()

        if License.objects.overlapping_licenses(self.customer, self.license_entity, self.start_date, new_date, self.pk).exists():
            raise exceptions.LicensePeriodIntersectionError()

        self.end_date = new_date
        self.save()

    class Meta:
        verbose_name = _('Лицензия')
        verbose_name_plural = _('Лицензии')
        default_related_name = 'licenses'


class LicenseEntity(TimestampedModel):
    name = models.CharField(max_length=255, verbose_name=_('Название'))
    description = RichTextField(**NULLABLE, verbose_name=_('Описание'))

    def __str__(self):
        return self.name

    @staticmethod
    def get_default():
        return LicenseEntity.objects.get_or_create(name='CRM')[0]

    class Meta:
        verbose_name = _('Объект лицензирования')
        verbose_name_plural = _('Объекты лицензирования')
