from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _

from common.models import NULLABLE
from .. import managers


class CustomUser(AbstractUser):
    objects = managers.CustomUserManager()

    customer = models.ForeignKey(
        to='core.Customer',
        verbose_name=_('Клиент'),
        on_delete=models.CASCADE,
        related_name='accounts',
        **NULLABLE,
    )

    is_partner_account = models.BooleanField(verbose_name=_('Партнерский аккаунт'), default=False)
    email = models.EmailField(_('email address'), **NULLABLE)

    def get_customer(self):
        try:
            return self.customer

        except:
            return None

    def __str__(self):
        return f"{self.customer}: {self.username}"


# TODO: migrations; docker; tests