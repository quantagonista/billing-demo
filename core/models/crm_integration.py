import random
from hashlib import md5

from django.db.models import (
    OneToOneField, DateTimeField, CharField,
    URLField, Model, CASCADE, BooleanField
)
from django.utils.translation import gettext_lazy as _

from core.models import CustomUser


class ExternalCrmIntegration(Model):
    user = OneToOneField(CustomUser, verbose_name=_("Клиент"), on_delete=CASCADE, related_name="x_integration")
    created_at = DateTimeField(auto_now_add=True, verbose_name=_("Дата добавления"))
    value = CharField(max_length=100, verbose_name=_("Токен"), unique=True, blank=True)
    crm_url = URLField(verbose_name=_("Ссылка в CRM клиента"))

    def save(self, *args, **kwargs):
        if not self.value:
            self.value = md5(str(random.getrandbits(128)).encode()).hexdigest()
        return super().save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Внешние интеграции с CRM"
        verbose_name = "Внешняя интеграция с CRM"


class CrmServer(Model):
    name = CharField(max_length=100, verbose_name=_('Название'))
    url = URLField(verbose_name=_('Ссылка в CRM'))
    auth_token = CharField(verbose_name=_('Секретный ключ'), max_length=255, help_text=_('для доступа к API CRM(CRM_ADMIN_AUTH_TOKEN)'))
    is_default = BooleanField(verbose_name=_('Выбирать по умолчанию'), default=False, help_text=_('При создании клиентов, копия CRM системы будет создаваться на этом сервере'))

    class Meta:
        verbose_name = _('CRM сервер')
        verbose_name_plural = _('CRM сервера')

    def __str__(self):
        return self.name

    def save(self, **kwargs):
        if self.url.endswith('/'):
            self.url = self.url[:-1]

        if not self.name:
            self.name = self.url

        super().save(**kwargs)
