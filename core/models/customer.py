from ckeditor.fields import RichTextField
from django.db import models
from django.db.models import SET_NULL
from django.utils.translation import gettext_lazy as _

from common.models import TimestampedModel, NULLABLE
from . import CustomUser, CrmServer
from .choices import CustomerStatuses, OrgTypesEnum, CrmStatuses

status_choices, default_status = CustomerStatuses.choices, CustomerStatuses.INACTIVE
crm_status_choices, default_crm_status = CrmStatuses.choices, CrmStatuses.UNKNOWN


class Customer(TimestampedModel):
    type = models.ForeignKey('CustomerType', SET_NULL, verbose_name=_('Тип CRM'), null=True, blank=False)
    assignees = models.ManyToManyField(CustomUser, 'assigned_customers', verbose_name=_('Ответственные'))
    created_by = models.ForeignKey(CustomUser, SET_NULL, 'created_customers', **NULLABLE, verbose_name=_('Кем добавлен'))

    status = models.CharField(max_length=50, choices=status_choices, verbose_name=_('Статус'), default=default_status)

    name = models.CharField(max_length=255, verbose_name=_('Название организации'))
    description = RichTextField(verbose_name=_('Описание'), **NULLABLE)
    url = models.URLField(verbose_name=_('Сайт компании'), **NULLABLE)

    server = models.ForeignKey(CrmServer, on_delete=SET_NULL, related_name='customers', **NULLABLE)
    org_id = models.IntegerField(verbose_name=_("ID Организации на сервере"), **NULLABLE)

    crm_status = models.CharField(verbose_name='CRM Status', choices=crm_status_choices, default=default_crm_status, max_length=20)
    users_count = models.PositiveSmallIntegerField(verbose_name=_('Масштаб компании/Кол-во пользователей'))

    contact_phone_number = models.CharField(max_length=255, verbose_name=_('Номер телефона'), **NULLABLE)
    contact_person = models.CharField(max_length=255, verbose_name=_('Контактное лицо'), **NULLABLE)
    contact_skype = models.CharField(max_length=255, verbose_name='Skype', **NULLABLE)
    contact_email = models.EmailField(verbose_name=_('Почта'), **NULLABLE)

    def __str__(self):
        return self.name

    def get_current_license(self):
        return self.licenses.order_by('-end_date').first()

    def has_crm_integration(self):
        user = self.accounts.first()
        return hasattr(user, 'x_integration')

    def get_crm_integration(self):
        user = self.accounts.first()
        return getattr(user, 'x_integration', None)

    def remove_server(self):
        self.server = None
        self.save()

    class Meta:
        verbose_name = _('Клиент')
        verbose_name_plural = _('Клиенты')
        default_related_name = 'customers'


class CustomerType(models.Model):
    name = models.CharField(max_length=100, verbose_name=_('Название'))
    code = models.CharField(max_length=100, choices=OrgTypesEnum.choices, verbose_name=_('Внутренний код'), null=True, blank=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Тип клиента')
        verbose_name_plural = _('Типы клиентов')
