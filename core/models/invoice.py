from base64 import b64encode, b64decode
from contextlib import suppress
from typing import Optional

from ckeditor.fields import RichTextField
from django.db import models, transaction
from django.db.models import signals
from django.dispatch import receiver
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from polymorphic.models import PolymorphicModel

from common.models import TimestampedModel, NULLABLE
from . import Customer, License
from .. import exceptions
from ..managers.invoice import InvoiceManager


class Invoice(TimestampedModel):
    objects = InvoiceManager()

    customer = models.ForeignKey(to=Customer, on_delete=models.PROTECT, related_name='invoices', verbose_name=_('Клиент'))

    title = models.CharField(max_length=255, verbose_name=_('Заголовок'))
    initial_amount = models.DecimalField(verbose_name=_('Сумма без учета скидки $'), max_digits=15, decimal_places=2, default=0)
    discount_amount = models.DecimalField(verbose_name=_('Сумма скидки $'), max_digits=15, decimal_places=2, default=0)
    final_amount = models.DecimalField(verbose_name=_('Итоговая сумма $'), max_digits=15, decimal_places=2, default=0)
    is_paid = models.BooleanField(default=False, verbose_name=_('Оплачено?'))
    payment_date = models.DateTimeField(verbose_name=_('Дата оплаты'), **NULLABLE)
    last_notified_at = models.DateTimeField(verbose_name=_("Последнее напоминание"), **NULLABLE)

    def __str__(self):
        return f"{self.customer}-{self.title}: {self.final_amount} $"

    @property
    def slug(self):
        decoded_slug = b64encode(f"inv:{self.id}".encode("utf-8")).decode("utf-8")
        decoded_slug = decoded_slug.replace("=", "_")
        return decoded_slug

    @staticmethod
    def parse_slug(slug_encoded) -> Optional[int]:
        """
        returns invoice id
        """
        try:
            slug_encoded = slug_encoded.replace("_", "=")
            slug = b64decode(slug_encoded.encode("utf-8")).decode("utf-8")
            if slug.startswith("inv:"):
                return int(slug[4:])
        except:
            pass

    @transaction.atomic
    def save(self, **kwargs):
        self.initial_amount = self.initial_amount or 0
        self.discount_amount = self.discount_amount or 0

        if self.discount_amount > self.initial_amount:
            raise exceptions.InvalidDiscountAmountError()

        self.final_amount = self.initial_amount - self.discount_amount

        super().save(**kwargs)

    def recalculate_amount(self):
        total_initial_amount = total_discount_amount = total_final_amount = 0

        for item in self.items.all():
            total_initial_amount += item.initial_amount
            total_discount_amount += item.discount_amount
            total_final_amount += item.final_amount

        self.initial_amount = total_initial_amount
        self.discount_amount = total_discount_amount
        self.final_amount = total_final_amount

        self.save()

    def mark_as_paid(self, payment_date=None):
        self.is_paid = True
        self.payment_date = payment_date or timezone.now()
        self.save()

    def mark_as_unpaid(self):
        self.is_paid = False
        self.payment_date = None
        self.save()

    class Meta:
        ordering = ["-created_at"]
        verbose_name = _('Счет на оплату')
        verbose_name_plural = _('Счета на оплату')


@receiver(signals.post_save, sender=Invoice)
def prolong_licences_if_needed(sender, instance, **kwargs):
    if instance.is_paid:
        for item in instance.items.instance_of(LicenseInvoiceItem):
            with suppress(exceptions.InvalidLicenseDateError):
                item.license.prolong_license(item.end_date)


class InvoiceItem(PolymorphicModel):
    invoice = models.ForeignKey(Invoice, on_delete=models.CASCADE, related_name='items', verbose_name=_('Счет на оплату'))

    title = models.CharField(max_length=255, verbose_name=_('Заголовок'))
    initial_amount = models.DecimalField(verbose_name=_('Сумма без учета скидки $'), max_digits=15, decimal_places=2)
    discount_amount = models.DecimalField(verbose_name=_('Сумма скидки $'), max_digits=15, decimal_places=2)
    final_amount = models.DecimalField(verbose_name=_('Итоговая сумма $'), max_digits=15, decimal_places=2)
    description = RichTextField(verbose_name=_('Описание'), **NULLABLE)

    def __str__(self):
        return f"{self.invoice} > {self.title}"

    def save(self, *args, **kwargs):
        if self.discount_amount > self.initial_amount:
            raise exceptions.InvalidDiscountAmountError()

        self.final_amount = self.initial_amount - self.discount_amount
        super().save(**kwargs)

    class Meta:
        verbose_name = _('Обьект счета на оплату')
        verbose_name_plural = _('Обьекты счета на оплату')


@receiver(signals.post_save, sender=InvoiceItem)
@receiver(signals.post_delete, sender=InvoiceItem)
def recalculate_invoice_amount(sender, instance, **kwargs):
    instance.invoice.recalculate_amount()


class LicenseInvoiceItem(InvoiceItem):
    license = models.ForeignKey(to=License, on_delete=models.PROTECT, related_name='invoice_items', verbose_name=_('Лицензия'))

    start_date = models.DateField(verbose_name=_('Начало периода'))
    end_date = models.DateField(verbose_name=_('Конец периода'))

    def __str__(self):
        return f"{self.invoice} > {self.license}"

    class Meta:
        verbose_name = _('Обьект счета на оплату: Лицензия')
        verbose_name_plural = _('Обьекты счета на оплату: Лицензия')


# Propagate signal across all children of a polymorphic parent.
# This code should be after declaring all of InvoiceItem children
for subclass in InvoiceItem.__subclasses__():
    signals.post_save.connect(recalculate_invoice_amount, sender=subclass)
    signals.post_delete.connect(recalculate_invoice_amount, sender=subclass)
