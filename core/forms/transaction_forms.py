from django import forms

from core import models, constants as const


class TransactionAdminForm(forms.ModelForm):
    def clean(self):
        type_ = self.cleaned_data.get('type')
        invoice = self.cleaned_data.get('invoice')
        customer = self.cleaned_data.get('customer')
        amount = self.cleaned_data.get('amount', 0)

        if type_ == models.Transaction.CREDIT:
            if models.Transaction.objects.get_customer_balance(customer=customer) < amount:
                self.add_error('amount', const.INVALID_CREDIT_TRANSACTION_AMOUNT_ERROR)

            if not invoice:
                self.add_error('invoice', const.INVALID_INVOICE_FOR_CREDIT_TRANSACTION)

            elif invoice.is_paid:
                self.add_error('invoice', const.INVALID_TRANSACTION_FOR_PAID_INVOICE)

            elif amount != invoice.final_amount:
                self.add_error('amount', const.INVALID_CREDIT_TRANSACTION_FOR_INVOICE)

        if type_ == models.Transaction.DEBIT and invoice:
            self.add_error('invoice', const.INVALID_INVOICE_FOR_DEBIT_TRANSACTION)

        return self.cleaned_data

    class Meta:
        model = models.Transaction
        exclude = ('is_canceled',)
