from django import forms
from django.contrib.auth import password_validation
from django.contrib.auth.forms import UsernameField
from django.utils.translation import gettext_lazy as _

from common.forms import CrmForm, CrmModelForm
from core.models import CustomerType, Customer


class CustomerRegistrationForm(CrmModelForm):
    contact_email = forms.EmailField(label='Email', required=True)
    agreement_accepted = forms.BooleanField(required=True)

    def save(self, commit=True):
        return super().save(commit)

    class Meta:
        model = Customer
        fields = ('name', 'type', 'contact_email', 'agreement_accepted')


class CustomerUserCreateForm(CrmForm):
    first_name = forms.CharField(label=_('Имя'))
    last_name = forms.CharField(label=_('Фамилия'))
    username = UsernameField(label=_('Имя пользователя'), widget=forms.TextInput(attrs={'autofocus': True}))
    password = forms.CharField(
        label=_("Пароль"), strip=False,
        widget=forms.PasswordInput(attrs={'autocomplete': 'current-password'}),
        help_text=password_validation.password_validators_help_text_html()
    )
    confirm_password = forms.CharField(
        label=_("Повторите пароль"), strip=False,
        widget=forms.PasswordInput(attrs={'autocomplete': 'current-password'}),
    )

    def clean(self):
        if self.cleaned_data.get('password') != self.cleaned_data.get('confirm_password'):
            self.add_error('password', _('Пароли не совпадают'))

        password_validation.validate_password(self.cleaned_data.get('confirm_password'))
        return self.cleaned_data
