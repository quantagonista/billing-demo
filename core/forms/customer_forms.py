from django import forms
from django.forms import ModelChoiceField, CharField, DateField, DecimalField, IntegerField
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from common.forms import CrmModelForm, CrmForm
from core import models
from core.constants import (
    CUSTOMER_BY_EMAIL_EXISTS, CUSTOMER_BY_NAME_EXISTS, INVALID_CUSTOMER_FULL_NAME
)
from core.models import LicenseEntity, CustomerType, CrmServer


class CustomerFastCreateForm(CrmModelForm):
    """ Used to create Customer with License and first Invoice """

    contact_person = CharField(label=_('Контактное лицо'), help_text=_('Введите фамилию и имя через пробел'))
    contact_email = CharField(
        label='Email',
        help_text=_('На основе email будет сгенерированы данные для входа в CRM')
    )

    users_count = IntegerField(label=_('Кол-во пользователей'), initial=100)
    type = ModelChoiceField(label=_('Тип CRM'), queryset=CustomerType.objects.all())
    crm_server = ModelChoiceField(label=_('CRM Сервер'), queryset=CrmServer.objects.all())

    # License
    license_entity = ModelChoiceField(label=_('Что покупает'), queryset=LicenseEntity.objects.all())
    license_start_date = DateField(label=_('Начало пользования системой'), initial=timezone.now().date())
    license_price = DecimalField(label=_('Стоимость в месяц после скидок $'), max_digits=10, decimal_places=0,
                                 initial=29, min_value=0, help_text=_('Сколько будет платить когда скидки закончатся'))

    # First payment
    first_payment = DecimalField(label=_('Первый платеж $'), max_digits=10, decimal_places=0, min_value=0, initial=29)
    first_payment_months = IntegerField(label=_('За сколько месяцев'), initial=1, min_value=0,
                                        help_text=_('С учетом бесплатных периодов'))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['license_entity'].initial = LicenseEntity.objects.filter(name__icontains='CRM').first()
        self.fields['type'].initial = CustomerType.objects.filter(code='courses').first()
        self.fields['crm_server'].initial = CrmServer.objects.filter(is_default=True).first()

    def clean(self):
        contact_person = self.cleaned_data.get('contact_person')

        if len(contact_person.split(' ')) != 2:
            self.add_error('contact_person', INVALID_CUSTOMER_FULL_NAME)

        customer_name = self.cleaned_data.get('name')
        customer_email = self.cleaned_data.get('contact_email')

        if models.Customer.objects.filter(name=customer_name).exists():
            self.add_error("name", CUSTOMER_BY_NAME_EXISTS)

        if models.Customer.objects.filter(contact_email=customer_email).exists():
            self.add_error("contact_email", CUSTOMER_BY_EMAIL_EXISTS)

        return super().clean()

    class Meta:
        model = models.Customer
        fields = ('name', 'type', 'users_count', 'url', 'contact_phone_number', 'contact_person', 'contact_skype', 'contact_email')


class CustomerCreateForm(CrmModelForm):
    contact_person = CharField(label=_('Контактное лицо'))
    contact_email = CharField(label='Email')

    class Meta:
        model = models.Customer
        fields = ('name', 'type', 'url', 'contact_phone_number', 'contact_person', 'contact_skype', 'contact_email')

    def clean(self):
        contact_person = self.cleaned_data.get('contact_person')

        if len(contact_person.split(' ')) != 2:
            self.add_error('contact_person', INVALID_CUSTOMER_FULL_NAME)

        customer_name = self.cleaned_data.get('name')
        customer_email = self.cleaned_data.get('contact_email')

        if models.Customer.objects.filter(name=customer_name).exists():
            self.add_error("name", CUSTOMER_BY_NAME_EXISTS)

        if models.Customer.objects.filter(contact_email=customer_email).exists():
            self.add_error("contact_email", CUSTOMER_BY_EMAIL_EXISTS)

        return super().clean()


class CustomerUpdateForm(CrmModelForm):
    type = ModelChoiceField(label=_('Тип CRM'), queryset=CustomerType.objects.all())
    description = CharField(label=_('Описание'), required=False, widget=forms.Textarea({'rows': 3, 'cols': '100%'}))
    server = ModelChoiceField(label=_('CRM Сервер'), queryset=CrmServer.objects.all())

    class Meta:
        model = models.Customer
        fields = ('name', 'type', 'description', 'users_count',
                  'url', 'contact_phone_number', 'contact_person',
                  'contact_skype', 'contact_email', 'server')


class CustomerExternalIntegrationForm(CrmForm):
    crm_server = ModelChoiceField(label=_('CRM Сервер'), queryset=CrmServer.objects.all())


class CustomerActivateForm(CrmForm):
    license_entity = ModelChoiceField(label=_('Что покупает'), queryset=LicenseEntity.objects.all())
    start_date = DateField(label=_('Начало пользования системой'), initial=timezone.now().date())
    price = DecimalField(label=_('Стоимость в месяц после скидок $'), max_digits=10, decimal_places=0,
                         initial=29, min_value=0, help_text=_('Сколько будет платить когда скидки закончатся'))

    first_payment = DecimalField(label=_('Первый платеж $'), max_digits=10, decimal_places=0, min_value=0, initial=29)
    first_payment_months = IntegerField(label=_('За сколько месяцев'), initial=1, min_value=0,
                                        help_text=_('С учетом бесплатных периодов'))

    def __init__(self, customer: models.Customer, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['license_entity'].initial = LicenseEntity.objects.filter(name__icontains='CRM').first()

        current_license = customer.get_current_license()
        current_date = timezone.now().date()

        if current_license and current_date <= current_license.end_date:
            self.fields['start_date'].help_text = _('Важно: Выбрана дата окончания предыдущей лицензии')
            self.fields['start_date'].initial = current_license.end_date + timezone.timedelta(days=1)

    class Meta:
        fields = ('license_entity', 'start_date', 'price', 'first_payment', 'first_payment_months')
