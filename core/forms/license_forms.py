from datetime import date

from django import forms
from django.core.exceptions import NON_FIELD_ERRORS

from common.forms import CrmModelForm
from core import models, constants as const


class LicenseAdminFormSet(forms.BaseInlineFormSet):
    def clean(self):
        valid_forms = [form for form in self.forms if form.is_valid() and form not in self.deleted_forms]

        mapper = {}

        for form in valid_forms:
            license_entity, start, end = self.get_license_period_from(form)

            if license_entity not in mapper:
                mapper[license_entity] = (start, end)
                continue

            existing_start, existing_end = mapper[license_entity]

            if self.periods_are_overlapping_each_other(existing_start, existing_end, start, end):
                form._errors[NON_FIELD_ERRORS] = self.error_class([self.get_form_error()])

            mapper[license_entity] = (existing_start, existing_end)

    @staticmethod
    def get_license_period_from(form):
        return (
            form.cleaned_data.get('license_entity'),
            form.cleaned_data.get('start_date'),
            form.cleaned_data.get('end_date'),
        )

    @staticmethod
    def periods_are_overlapping_each_other(existing_start: date, existing_end: date, start: date, end: date):
        return (existing_start <= start <= existing_end) or (existing_start <= end <= existing_end)

    def get_form_error(self):
        return const.LICENSE_PERIOD_INTERSECTION_ERROR


class LicenseAdminForm(forms.ModelForm):
    class Meta:
        model = models.License
        exclude = []

    def clean(self):
        license_entity = self.cleaned_data.get('license_entity')
        start_date = self.cleaned_data.get('start_date')
        end_date = self.cleaned_data.get('end_date')
        customer = self.cleaned_data.get('customer')

        if (models.License.objects.
                overlapping_licenses(customer, license_entity, start_date, end_date, self.instance.id).exists()):
            self.add_error(None, const.LICENSE_PERIOD_INTERSECTION_ERROR)

        return self.cleaned_data


class LicenseCreateForm(CrmModelForm):
    class Meta:
        model = models.License
        fields = (
            'customer',
            'license_entity',
            'type',
            'start_date',
            'end_date',
            'price',
            'discount_percentage',
            'description',
            'active',
        )

    def clean(self):
        license_entity = self.cleaned_data.get('license_entity')
        start_date = self.cleaned_data.get('start_date')
        end_date = self.cleaned_data.get('end_date')
        customer = self.cleaned_data.get('customer')

        if models.License.objects.overlapping_licenses(customer, license_entity, start_date, end_date, self.instance.id).exists():
            self.add_error(None, const.LICENSE_PERIOD_INTERSECTION_ERROR)

        return self.cleaned_data


class LicenseUpdateForm(LicenseCreateForm):
    class Meta(LicenseCreateForm.Meta):
        pass
