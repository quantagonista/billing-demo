# Used to reduce boilerplate for disabled form fields
DISABLED = {'disabled': True, 'required': False}
