import datetime

from ckeditor.fields import RichTextFormField
from django import forms
from django.utils.translation import gettext_lazy as _

from common.forms import CrmForm, CrmModelForm
from core import models, constants as const
from . import DISABLED


class InvoiceAdminForm(forms.ModelForm):
    initial_amount = forms.DecimalField(label=_('Изначальная сумма'), **DISABLED)
    discount_amount = forms.DecimalField(label=_('Сумма скидки'), **DISABLED)
    is_paid = forms.BooleanField(label=_('Оплачен?'), **DISABLED)
    payment_date = forms.DateTimeField(label=_('Дата оплаты'), **DISABLED)

    class Meta:
        model = models.Invoice
        exclude = ("final_amount",)

    def clean(self):
        is_paid = self.cleaned_data.get('is_paid')
        payment_date = self.cleaned_data.get('payment_date')

        if is_paid and not payment_date:
            self.add_error('payment_date', const.INVALID_PAYMENT_DATE_ERROR)

        return self.cleaned_data


class InvoiceItemAdminForm(forms.ModelForm):
    class Meta:
        model = models.InvoiceItem
        exclude = ('final_amount',)

    def clean(self):
        initial_amount = self.cleaned_data.get('initial_amount', 0)
        discount_amount = self.cleaned_data.get('discount_amount', 0)

        if discount_amount > initial_amount:
            self.add_error('discount_amount', const.INVALID_DISCOUNT_AMOUNT_ERROR)

        return self.cleaned_data


class LicenseInvoiceItemAdminForm(InvoiceItemAdminForm):
    def clean(self):
        super().clean()

        end_date = self.cleaned_data.get('end_date')
        license_ = self.cleaned_data.get('license')

        if models.License.objects.overlapping_licenses(
                customer=license_.customer,
                license_entity=license_.license_entity,
                start_date=license_.start_date,
                end_date=end_date,
                license_id=license_.pk).exists():
            self.add_error('end_date', const.LICENSE_PERIOD_INTERSECTION_ERROR)

    class Meta:
        model = models.LicenseInvoiceItem
        exclude = ('final_amount',)


class InvoiceItemForm(CrmModelForm):
    description = forms.CharField(label=_('Описание'), widget=forms.Textarea({'cols': '100%', 'rows': 3}), required=False)

    def __init__(self, *args, **kwargs):
        if kwargs.get('invoice'):
            kwargs.pop('invoice')

        super().__init__(*args, **kwargs)

    class Meta:
        model = models.InvoiceItem
        fields = (
            'title',
            'description',
            'initial_amount',
            'discount_amount',
        )

    def clean(self):
        initial_amount = self.cleaned_data.get('initial_amount', 0)
        discount_amount = self.cleaned_data.get('discount_amount', 0)

        if discount_amount > initial_amount:
            self.add_error('discount_amount', const.INVALID_DISCOUNT_AMOUNT_ERROR)

        return self.cleaned_data


class LicenceInvoiceItemForm(InvoiceItemForm):
    def __init__(self, *args, **kwargs):
        invoice = kwargs.pop('invoice')
        super().__init__(*args, **kwargs)
        self.fields['license'].queryset = models.License.objects.filter(customer=invoice.customer)

    class Meta:
        model = models.LicenseInvoiceItem
        fields = (
            'title',
            'description',
            'license',
            'initial_amount',
            'discount_amount',
            'start_date',
            'end_date',
        )

    def clean(self):
        end_date = self.cleaned_data.get('end_date')
        license_ = self.cleaned_data.get('license')

        if models.License.objects.overlapping_licenses(
                customer=license_.customer,
                license_entity=license_.license_entity,
                start_date=license_.start_date,
                end_date=end_date,
                license_id=license_.pk).exists():
            self.add_error('end_date', const.LICENSE_PERIOD_INTERSECTION_ERROR)

        return self.cleaned_data


class InvoicePayForm(CrmForm):
    invoice = forms.ModelChoiceField(models.Invoice.objects.all(), label=_('Счет'), disabled=True)
    payment_method = forms.ModelChoiceField(models.PaymentMethod.objects.filter(is_active=True), label=_('Способ оплаты'))
    amount = forms.DecimalField(label=_('Сколько оплатил клиент ($)'), help_text=_("Если клиент оплатил меньше указанной суммы, то счет не будет оплачен"))
    payment_date = forms.DateField(label=_('Дата платежа'), initial=datetime.date.today, required=True)


class InvoiceCreateForm(CrmForm):
    license = forms.ModelChoiceField(label=_('Лицензия'), queryset=models.License.objects.none())
    start_date = forms.DateField(label=_('Дата начала'))
    end_date = forms.DateField(label=_('Дата окончания'))
    initial_amount = forms.DecimalField(label=_('Сумма без скидки $'), initial=29)
    discount_amount = forms.DecimalField(label=_('Скидка $'), initial=0)
    description = RichTextFormField(required=False)

    def __init__(self, customer: models.Customer, *args, **kwargs):
        super().__init__(*args, **kwargs)

        license = customer.get_current_license()

        self.fields['license'].queryset = customer.licenses.filter(active=True).all()
        self.fields['license'].initial = license

        self.fields['start_date'].initial = license.end_date
        self.fields['end_date'].initial = license.end_date + license.length

    def clean(self):
        initial_amount = self.cleaned_data.get('initial_amount')
        discount_amount = self.cleaned_data.get('discount_amount')

        if discount_amount > initial_amount:
            self.add_error('discount_amount', const.INVALID_DISCOUNT_AMOUNT_ERROR)

        return self.cleaned_data

    class Meta:
        fields = ('license', 'start_date', 'end_date', 'initial_amount', 'discount_amount', 'description')
