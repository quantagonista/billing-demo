function onReady() {
    Navigation.init()
    Notifications.init()
    ConfirmButtons.init()
    Clickable.init()
    Tabs.init()
    Spoiler.init()
    StoreSpoiler.init()
}

$(document).ready(onReady)

let Tabs = {
    init: function () {
        $(".tabs").each(function () {
            $(this).find("li").on("click", function () {
                $(this).closest(".tabs").find("li").removeClass("is-active")
                $(this).addClass("is-active")
                $(this).closest(".tabs-container").find(".tab-content").addClass("is-hidden")
                let tabId = $(this).attr("id")
                $("#" + tabId + "-content").removeClass("is-hidden")
            })
        })
    }
}

let Spoiler = {
    init: function () {
        let ids = Spoiler.getExpandedIds()
        $(".spoiler .spoiler-content").hide()
        for (let id of ids) {
            if (id.trim() !== "") {
                $("#" + id + ".spoiler").addClass("is-expanded");
                $("#" + id + ".spoiler").find(".spoiler-content").show()
            }
        }
        $(".spoiler .spoiler-trigger").on("click", function () {
            let $parent = $(this).closest(".spoiler")
            let id = $parent.attr("id")
            $parent.toggleClass("is-expanded")

            if ($parent.hasClass("is-expanded")) {
                if (id != null) {
                    Spoiler.addExpandedId(id)
                }
                $parent.find(".spoiler-content").show()
            } else {
                if (id != null) {
                    Spoiler.removeExpandedId(id)
                }
                $parent.find(".spoiler-content").hide()
            }
        })
    },

    getExpandedIds: function () {
        let ids = localStorage.getItem("ExpandedSpoilerIds");
        if (ids != null) {
            return ids.split(",")
        }
        return []
    },

    addExpandedId: function (id) {
        let ids = Spoiler.getExpandedIds()
        ids += id + ","
        localStorage.setItem("ExpandedSpoilerIds", ids)
    },

    removeExpandedId: function (id) {
        let ids = Spoiler.getExpandedIds()
        const index = ids.indexOf(id);
        if (index > -1) {
            ids.splice(index, 1);
        }
        localStorage.setItem("ExpandedSpoilerIds", ids.join(","))
    }
}

let StoreSpoiler = {
    init: function () {
        let ids = StoreSpoiler.getHiddenIds()
        for (let id of ids) {
            StoreSpoiler.hide(id);
        }

        $(".store-spoiler").on("click", function () {
            let id = $(this).data("spoiler-id");
            if ($(this).hasClass("fa-minus-square")) {
                StoreSpoiler.hide(id);
                $(this).removeClass("fa-minus-square");
                $(this).addClass("fa-plus-square");
            } else {
                $(".group-" + id).show();
                $(".product-" + id).show();
                $(this).removeClass("fa-plus-square");
                $(this).addClass("fa-minus-square");
                StoreSpoiler.removeHiddenId(id)
            }
        });

        $(".collapse-all").on("click", function () {
            $(".group-0").each(function () {
                let id = $(this).data("id");
                StoreSpoiler.hide(id);
                $(this).find("i.store-spoiler").removeClass("fa-minus-square");
                $(this).find("i.store-spoiler").addClass("fa-plus-square");
            });
            return false;
        });

        $(".expand-all").on("click", function () {
            localStorage.setItem("HiddenStoreSpoilerIds", "")
            $("tr").show();
            $("i.store-spoiler").addClass("fa-minus-square");
            $("i.store-spoiler").removeClass("fa-plus-square");
            return false;
        })
    },

    hide: function (id) {
        $(".product-" + id).hide();
        $(".group-" + id).each(function () {
            StoreSpoiler.hide($(this).data("id"));
            $(this).hide();
            $(this).find("i.store-spoiler").removeClass("fa-minus-square");
            $(this).find("i.store-spoiler").addClass("fa-plus-square");
        });
        $(".store-spoiler[data-spoiler-id='" + id + "']").removeClass("fa-minus-square");
        $(".store-spoiler[data-spoiler-id='" + id + "']").addClass("fa-plus-square");
        StoreSpoiler.addHiddenId(id)
    },

    getHiddenIds: function () {
        let ids = localStorage.getItem("HiddenStoreSpoilerIds");
        if (ids != null) {
            return new Set(ids.split(","))
        }
        return new Set()
    },

    addHiddenId: function (id) {
        let ids = StoreSpoiler.getHiddenIds()
        ids.add(id.toString())
        localStorage.setItem("HiddenStoreSpoilerIds", Array.from(ids).join(","))
    },

    removeHiddenId: function (id) {
        let ids = StoreSpoiler.getHiddenIds()
        if (ids.has(id.toString())) {
            ids.delete(id.toString())
        }
        localStorage.setItem("HiddenStoreSpoilerIds", Array.from(ids).join(","))
    }
};

let Navigation = {
    init: function () {
        let currentPageId = $("#page-id").text().trim()
        if (currentPageId !== "") {
            $(".left-menu .item").removeClass("active")
            $(".left-menu .item[data-id='" + currentPageId + "']").addClass("active")
        }
    }
}

let Notifications = {
    init: function () {
        $(".notification.is-error").addClass("is-danger")
        $(document).on('click', ".notification>.delete", function () {
            $(this).closest(".notification").remove()
        })
        Notifications.setCloseTimeout();
    },

    setCloseTimeout: function () {
        setTimeout(function () {
            $(".notifications>.notification").remove()
        }, 5000)
    },

    add: function (type, message) {
        $(".notifications").append(
            "<div class=\"notification " + type + "\"><button class=\"delete\"></button>" + message + "</div>"
        )
        Notifications.setCloseTimeout();
    },

    addSuccess: function (message) {
        Notifications.add("is-success", message)
    },

    addError: function (message) {
        Notifications.add("is-error is-danger", message)
    },

    addInfo: function (message) {
        Notifications.add("is-info", message)
    },

    addWarning: function (message) {
        Notifications.add("is-warning", message)
    }
}

let WhatsApp = {
    makeChatLink: function (phone) {
        phone = phone.replace(/[+()\s]/g, "")
        if (phone.length === 10 && phone.startsWith("0")) {
            phone = "996" + phone.substring(1)
        }
        if (phone.length === 9) {
            phone = "996" + phone
        }
        return "https://wa.me/" + phone
    }
}

let ConfirmButtons = {
    init: function () {
        $(document).on("click", ".confirm-button", function () {
            let confirmText = $(this).data("confirm-text") || ""
            return confirm(gettext("Вы уверены?") + " " + confirmText)
        })
        $(document).on("click", ".delete-button", function () {
            return confirm(gettext("Вы уверены что хотите удалить?"))
        })
    }
}

let Clickable = {
    init: function () {
        $(".clickable.link").on("click", function () {
            let link = $(this).find("a").first().attr("href")
            if (link !== undefined) {
                window.location = link
            }
        })
        $('.burger').on("click", (function () {
            $('.left-menu').toggleClass("show")
        }))
        $('.close-burger').on("click", (function () {
            $('.left-menu').removeClass("show")
        }))
    }
}


let EventInfo = {
    init: function () {
        $(".event.event-row").on("click", function () {
            let eventId = $(this).data("object-id")
            let day = $(this).data("day")
            EventInfo.loadEventInfo(eventId, day)
        })
        $(".event-info-panel .panel-close").on("click", EventInfo.hidePanel)
    },

    showPanel: function () {
        $(".info-panel").hide()
        $(".event-info-panel").show()
    },

    hidePanel: function () {
        $(".event-info-panel").hide()
    },

    showProgress: function () {
        $(".event-info-panel .panel-progress").show()
        $(".event-info-panel .event-main-block").hide()
        $(".event-info-panel .join-event-link").hide()
        $(".event-info-panel #event-description").html("")
        $(".event-info-panel .edit-event-link").hide()
    },

    loadEventInfo: function (eventId, day) {
        EventInfo.showPanel()
        EventInfo.showProgress()

        $.get("/api/crm/events/" + eventId + "/", function (event) {
            EventInfo.showEventInfo(event, day)
        }).fail(function () {
            EventInfo.hidePanel()
        })
    },

    showEventInfo: function (event, day) {
        $(".event-info-panel .panel-progress").hide()
        $(".event-info-panel .event-main-block").show()
        $(".event-info-panel .event-title").text(event.title)
        $(".event-info-panel .event-subtitle").text(event.location)
        $(".event-info-panel .event-time").text(day)
        $(".event-info-panel .join-event-link a").css("background-color", event.color)
        $(".event-info-panel .event-color").css("background-color", event.color)

        if (event.meet_link != null) {
            $(".event-info-panel .join-event-link").show()
            $(".event-info-panel .join-event-link a").attr("href", event.meet_link)
        }
        $(".event-info-panel #event-description").html(event.description)
        $(".event-info-panel .edit-event-link").show()

        $(".event-info-panel .edit-event-link a").data("place", "form-fragment")
        $(".event-info-panel .edit-event-link a").data("url", "/events/ajax/events/" + event.id + "/edit/")
        $(".event-info-panel .edit-event-link a").data("modal", "event-form-modal")
    },
}

let LessonInfo = {
    init: function () {
        $(".event.lesson-row").on("click", function () {
            let courseId = $(this).data("course-id")
            let lessonId = $(this).data("object-id")
            let day = $(this).data("day")
            LessonInfo.loadCourseInfo(courseId, lessonId, day)
        })
        $(".lesson-info-panel .panel-close").on("click", LessonInfo.hidePanel)
        LessonInfo.initSingleToggle()
    },

    showPanel: function () {
        $(".info-panel").hide()
        $(".lesson-info-panel").show()
    },

    hidePanel: function () {
        $(".lesson-info-panel").hide()
        Students.hidePanel()
    },

    showProgress: function () {
        $(".lesson-info-panel .panel-progress").show()
        $(".lesson-info-panel .lesson-main-block").hide()
        $(".lesson-info-panel .join-lesson-link").hide()
    },

    loadCourseInfo: function (courseId, lessonId, day) {
        LessonInfo.showPanel()
        LessonInfo.showProgress()

        $.get("/old_api/courses/" + courseId + "/", function (course) {
            LessonInfo.showCourseInfo(course, day)
            LessonInfo.loadAttendanceInfo(course.students, lessonId)
        }).fail(function () {
            LessonInfo.hidePanel()
        })
    },

    loadAttendanceInfo: function (students, lessonId) {
        $.get("/lms/api/attendance/lesson/get/", {
            lesson_id: lessonId
        }, function (info) {
            LessonInfo.showAttendanceInfo(lessonId, students, info.attended_students, info.absences)
        }).fail(function () {

        })
    },

    showCourseInfo: function (course, day) {
        $(".lesson-info-panel .panel-progress").hide()
        $(".lesson-info-panel .lesson-main-block").show()
        $(".lesson-info-panel .course-title").text(course.name)
        $(".lesson-info-panel .course-mentor").text(course.mentor)
        $(".lesson-info-panel .course-time").text(day)
        $(".lesson-info-panel .join-lesson-link a").css("background-color", course.color)
        $(".lesson-info-panel .course-color").css("background-color", course.color)

        if (course.meet_link != null) {
            $(".lesson-info-panel .join-lesson-link").show()
            $(".lesson-info-panel .join-lesson-link a").attr("href", course.meet_link)
        }
        $(".lesson-info-panel .attendance-list").html("...")
    },

    showAttendanceInfo(lessonId, students, attended_students, absences) {
        let html = ""
        for (let student of students) {
            var absences_count = 0
            if (absences.hasOwnProperty(student.id)) {
                absences_count = absences[student.id]
            }
            let attended = attended_students.includes(student.id)
            html += LessonInfo.blocks.makeStudentAttendanceLine(lessonId, student.id, student.name, attended, absences_count)
        }
        $(".lesson-info-panel .attendance-list").html(html)
    },

    initSingleToggle: function () {
        $(document).on("click", ".attended-cell", function () {
            let $cell = $(this)
            let studentId = $(this).data("student")
            let lessonId = $(this).data("lesson")
            LessonInfo.toggleAttendance(studentId, lessonId, function (attended) {
                if (typeof attended === "boolean") {
                    if (attended) {
                        $cell.removeClass("is-danger")
                        $cell.addClass("is-success")
                        $cell.html(gettext("присутствовал"))
                    } else {
                        $cell.removeClass("is-success")
                        $cell.addClass("is-danger")
                        $cell.html(gettext("отсутствовал"))
                    }
                }
            })
        })
    },

    toggleAttendance: function (studentId, lessonId, callback) {
        $.post("/lms/api/attendance/toggle/", {
            "student_id": studentId,
            "lesson_id": lessonId
        }).done(function (data) {
            if (data.status === "SUCCESS") {
                callback(data.attended)
            } else {
                callback(null)
            }
        }).fail(function () {
            callback(null)
        })
    },

    blocks: {
        makeStudentAttendanceLine: function (lessonId, id, name, attended, absences_count) {
            let html = "<span class=\"tag is-rounded is-danger attended-cell\" data-student='" + id + "' data-lesson='" + lessonId + "'>" +
                gettext("отсутствовал") +
                "        </span>"
            if (attended) {
                html = "<span class=\"tag is-rounded is-success attended-cell\" data-student='" + id + "' data-lesson='" + lessonId + "'>" +
                    gettext("присутствовал") +
                    "        </span>"
            }

            let absence = "<span class=\"tag is-rounded is-link\">" +
                "        " + absences_count +
                "        </span>"

            return "<div class=\"student-line\">" +
                "                <div class=\"student-name cell\">" +
                "                    <i class=\"fas fa-user-circle\"></i>" +
                "                    <a href='#' class=\"name student-row\" data-id='" + id + "'>" + name + "</a>" +
                "                </div>" +
                "                <div class=\"attendance cell\">" +
                "                  " + html +
                "                </div>" +
                "                <div style='margin-left: 4px;' class=\"attendance cell\">" +
                "                  " + absence +
                "                </div>" +
                "            </div>"
        }
    }
}

let Mentors = {
    init: function () {
        $(".mentor-row").on("click", function () {
            let id = $(this).data("id")
            Mentors.loadMentorInfo(id)
        })
        $(".side-info-panel .panel-close").on("click", Mentors.hidePanel)
    },

    showPanel: function () {
        $(".side-info-panel").show()
    },

    hidePanel: function () {
        $(".side-info-panel").hide()
    },

    showProgress: function () {
        $("#mentor-progress").show()
        $("#mentor-main-block").hide()
        $("#mentor-contract-block").hide()
        $("#mentor-courses-block").hide()
        $("#mentor-invite-block").hide()
        $("#mentor-actions-block").hide()
    },

    showMentorInfo: function (mentor) {
        $("#mentor-progress").hide()
        $("#mentor-main-block").show()
        $("#mentor-contract-block").show()
        $("#mentor-actions-block").show()

        $("#mentor-name").text(mentor.first_name + " " + mentor.last_name)
        $("#mentor-avatar").attr("src", mentor.gravatar_url)
        $("#mentor-edit-link").attr("href", "/mentors/" + mentor.id + "/edit/")

        let $details = $("#mentor-details-block")
        $details.html("")

        $details.append(Mentors.blocks.createDetailsBlock(gettext("Дата добавления"), mentor.created_date))

        if (mentor.works_at != null) {
            $details.append(Mentors.blocks.createDetailsBlock(gettext("Место работы"), mentor.works_at))
        }
        if (mentor.email != null) {
            $details.append(Mentors.blocks.createDetailsBlock(gettext("Email"), mentor.email))
        }
        if (mentor.phone != null) {
            $details.append(Mentors.blocks.createDetailsBlock(gettext("Номер телефона"), mentor.phone))
        }
        if (mentor.birth_date != null) {
            $details.append(Mentors.blocks.createDetailsBlock(gettext("Дата рождения"), mentor.birth_date))
        }
        if (mentor.skills != null) {
            $details.append(Mentors.blocks.createDetailsBlock(gettext("Навыки"), mentor.skills))
        }
        if (mentor.account_info != null) {
            let info = gettext("Приглашение отправлено")
            if (mentor.account_info === "registered") {
                info = gettext("Зарегистрирован")
            }
            $details.append(Mentors.blocks.createDetailsBlock(gettext("Аккаунт"), info))
        }

        let $courses = $("#mentor-courses-block")
        $courses.show()
        $courses.html("")
        for (let course of mentor.active_courses) {
            $courses.append(Mentors.blocks.createCourseInfoBlock(course))
        }

        let $account = $("#mentor-invite-block")
        $account.html("")
        if (mentor.account_info == null || mentor.account_info === 'invited') {
            $account.show()
            $account.html(Mentors.blocks.createInviteBlock(mentor))
        } else if (mentor.account_info !== null && mentor.account_info === 'registered') {
            $account.show()
            $account.html(Mentors.blocks.createMentorPasswordChangeBlock(mentor))
        }

        let $actions = $("#mentor-actions-block")
        $actions.html("")
        if (mentor.hasOwnProperty("available_actions")) {
            for (let action of mentor.available_actions) {
                $actions.html(Mentors.blocks.createActionBlock(action))
            }
        }
    },

    blocks: {
        createDetailsBlock: function (key, value) {
            return "<div class=\"key-info\">\n" +
                "            <div class=\"key\">" + key + ":</div>\n" +
                "            <div class=\"value\"><div>" + value + "</div></div>\n" +
                "       </div>"
        },

        createCourseInfoBlock: function (course) {
            let progressBlock =
                "        <div class=\"single-info\">\n" +
                "            <progress class=\"months progress is-warning\" value=\"" + course.months_passed + "\" max=\"" + course.months + "\">30%</progress>\n" +
                "        </div>\n" +
                "        <div class=\"single-info\">\n" +
                interpolate(gettext("Пройдено %(passed)s месяцев из %(total)s"), {
                    passed: course.months_passed,
                    total: course.months,
                }, true) +
                "        </div>\n";
            if (course.status === "finished") {
                let endDate = ""
                if (course.end_date !== null) {
                    endDate = ": " + course.end_date;
                }
                progressBlock =
                    "<div class='single-info'>" +
                    "   <b>" + gettext("Курс завершен") + "</b>" + endDate +
                    "</div>"
            }

            return "<div class=\"side-block\">\n" +
                "        <div class=\"icon-btns\">" +
                "            <a href=\"" + course.link + "\"><i class='fa fa-arrow-right'></i></a>\n" +
                "        </div> " +
                "        <div>\n" +
                "            <div class=\"title-info\">\n" +
                "                " + gettext("Курс") + ": " + course.name +
                "            </div>\n" +
                "            <div class=\"schedule-info\">\n" +
                "                " + course.schedule +
                "            </div>\n" +
                "            <div class=\"students-count-info\">" + gettext("Студенты") + ": " + course.students_count + "/" + course.students_required + "</div>" +
                "            <div class=\"clearfix\"></div>\n" +
                "        </div>\n" +
                "        <div class=\"single-info\">\n" +
                "            " + gettext("Начало") + ": " + course.start_date + "\n" +
                "        </div>\n" +
                progressBlock +
                "    </div>"
        },

        createInviteBlock: function (mentor) {
            let html = "<div class=\"side-block\">\n" +
                "        <div style='position: relative;'>\n" +
                "            <div class=\"title-info\">\n" +
                gettext("Аккаунт") +
                "            </div>\n"
            let inviteUrl = "/mentors/" + mentor.id + "/invite/?next=" + document.location.href
            html += "    <a href=\"" + inviteUrl + "\" class=\"crm-button button-info\">\n" +
                gettext("Пригласить") +
                "        </a>\n"

            html += "        <div class=\"clearfix\"></div>\n" +
                "        </div>\n" +
                "    </div>"
            return html
        },

        createMentorPasswordChangeBlock: function (mentor) {
            let html = "<div class=\"side-block\">\n" +
                "        <div style='position: relative;'>\n" +
                "            <div class=\"title-info\">\n" +
                gettext("Аккаунт") +
                "            </div>\n"
            let changeUrl = "/ajax/mentors/" + mentor.id + "/change-password"
            html += "    <a data-url=\"" + changeUrl + "\" class=\"crm-button ajax-form button-info\" " +
                "               data-modal='change-password-form-modal' data-place='form-fragment'>\n" +
                gettext("Сбросить пароль") +
                "        </a>\n"

            html += "        <div class=\"clearfix\"></div>\n" +
                "        </div>\n" +
                "    </div>"
            return html
        },

        createActionBlock: function (action) {
            let html = "<div class=\"side-block\">\n" +
                "        <div style='position: relative;'>\n" +
                "            <div class=\"title-info\">\n" +
                "               " + action.title +
                "            </div>\n " +
                "        <a href=\"" + action.url + "\" class=\"crm-button button-info " + action.css_class + " \">\n" +
                "           " + action.button +
                "        </a>\n"

            html += "        <div class=\"clearfix\"></div>\n" +
                "        </div>\n" +
                "    </div>"
            return html
        }
    },

    loadMentorInfo: function (id) {
        Mentors.showPanel()
        Mentors.showProgress()

        $.get("/old_api/mentors/" + id + "/", function (mentor) {
            Mentors.showPanel()
            Mentors.showMentorInfo(mentor)
        }).fail(function () {
            Mentors.hidePanel()
        })
    }
}

let Students = {
    lastId: null,

    init: function () {
        $(document).on("click", ".student-row", function () {
            let id = $(this).data("id")
            Students.lastId = id
            Students.loadStudentInfo(id)
        })
        $(document).on("click", ".side-info-panel .panel-close", Students.hidePanel)
    },

    showPanel: function () {
        $(".side-info-panel").show()
        if ($(".lesson-info-panel").is(":visible")) {
            let right = $(".lesson-info-panel").outerWidth()
            $(".side-info-panel").css("right", right + "px")
        }
    },

    hidePanel: function () {
        $(".side-info-panel").hide()
        $(".side-info-panel").css("right", "0")
    },

    showProgress: function () {
        $("#student-progress").show()
        $("#student-main-block").hide()
        $("#student-contract-block").hide()
        $("#student-courses-block").hide()
        $("#student-invite-block").hide()
        $("#student-actions-block").hide()
        $("#students-parent-info-block").hide()
        $("#student-register-block").hide()
    },

    showStudentInfo: function (student) {
        $("#student-progress").hide()
        $("#student-main-block").show()
        $("#student-contract-block").show()
        $("#student-actions-block").show()
        $("#students-parent-info-block").show()
        $("#student-invite-block").show()
        $("#student-register-block").show()

        $("#student-name").text(student.first_name + " " + student.last_name)
        $("#student-avatar").attr("src", student.gravatar_url)

        $("#student-edit-link").data("url", "/ajax/students/" + student.id + "/edit/")
        $("#student-edit-link").data("place", "form-fragment")
        $("#student-edit-link").data("modal", "student-form-modal")

        let editStudentParentUrl = "/students/" + student.id + "/parents/"
        $("#student-parent-edit-link").attr("href", editStudentParentUrl)

        let $details = $("#student-details-block")
        $details.html("")

        $details.append(Students.blocks.createDetailsBlock(gettext("Дата добавления"), student.created_date))
        $details.append(Students.blocks.createDetailsBlock(gettext("Статус"), student.get_status_display))

        if (student.came_from != null) {
            $details.append(Students.blocks.createDetailsBlock(gettext("Откуда"), student.came_from))
        }
        if (student.email != null) {
            $details.append(Students.blocks.createDetailsBlock(gettext("Email"), student.email))
        }
        if (student.phone != null) {
            let whatsAppLink = WhatsApp.makeChatLink(student.phone)
            let icon = Students.blocks.createLinkImg(whatsAppLink, '/static/img/whatsapp.png')
            $details.append(Students.blocks.createDetailsBlock(gettext("Номер телефона"), student.phone, icon))
        }
        if (student.birth_date != null) {
            $details.append(Students.blocks.createDetailsBlock(gettext("Дата рождения"), student.birth_date))
        }
        if (student.get_gender_display != null) {
            $details.append(Students.blocks.createDetailsBlock(gettext("Пол"), student.get_gender_display))
        }
        for (let extraField of student.extra_fields) {
            $details.append(Students.blocks.createDetailsBlock(extraField.name, extraField.value))
        }
        if (student.converted_by != null) {
            $details.append(Students.blocks.createDetailsBlock(gettext("Кем добавлен или переведен с лида"), student.converted_by))
        }
        if (student.account_info != null) {
            let info = gettext("Приглашение отправлено")
            if (student.account_info === "registered") {
                info = gettext("Зарегистрирован")
            }
            $details.append(Students.blocks.createDetailsBlock(gettext("Аккаунт"), info))
        }

        let $parents = $("#student-parents-block")
        $parents.html("")

        if (student.parents != null) {
            for (let parent of student.parents) {
                $parents.append(Students.blocks.createParentsBlock(parent.first_name, parent.last_name))
            }
        }

        let $courses = $("#student-courses-block")
        $courses.show()
        $courses.html("")
        let bridgeHtml = "<div class='bridge'><hr><hr><hr><hr><hr><hr><hr><hr><hr><hr><hr><hr><hr></div>"
        for (let course of student.active_courses) {
            let courseBlock = "<div class='side-group-block'>"
            courseBlock += Students.blocks.createCourseHeaderBlock(course) + bridgeHtml
            courseBlock += Students.blocks.createCourseInfoBlock(student, course) + bridgeHtml
            if (course.payment_info !== null) {
                courseBlock += Students.blocks.createPaymentBlock(course, student, course.payment_info) + bridgeHtml
            }
            courseBlock += Students.blocks.createContractBlock(course, student, course.contract_info)
            courseBlock += "</div>"
            $courses.append(courseBlock)
        }

        let $account = $("#student-invite-block")
        $account.html("")
        if (student.account_info == null || student.account_info === 'invited') {
            $account.show()
            $account.html(Students.blocks.createInviteBlock(student))
        } else if (student.account_info !== null && student.account_info === 'registered') {
            $account.show()
            $account.html(Students.blocks.createStudentPasswordChangeBlock(student))
        }

        let $register = $("#student-register-block")
        $register.html(Students.blocks.createRegisterBlock(student))

        let $actions = $("#student-actions-block")
        $actions.html("")
        if (student.hasOwnProperty("available_actions")) {
            for (let action of student.available_actions) {
                $actions.html(Students.blocks.createActionBlock(action))
            }
        }
    },

    updateStudentOnPage: function (student) {
        let $studentItem = $("#student-" + student.id);
        $studentItem.find(".name").text(student.first_name + " " + student.last_name)

        if (student.phone != null) {
            $studentItem.find(".phone").text(student.phone)
        } else {
            $studentItem.find(".phone").text("")
        }
    },

    blocks: {
        createDetailsBlock: function (key, value, icon = "") {
            return "<div class=\"key-info\">\n" +
                "            <div class=\"key\">" + key + ":</div>\n" +
                "            <div class=\"value\"><div>" + value + "</div>" + icon + "</div>\n" +
                "       </div>"
        },

        createParentsBlock: function (parent_name, parent_lastname) {
            return "<div class=\"single-info\">\n" +
                "            <div class=\"key\">" + parent_name + " " + parent_lastname + "</div>\n" +
                "       </div>"
        },

        createLinkImg: function (url, image_url) {
            return "<a target='_blank' href='" + url + "'><img class='icon' src='" + image_url + "' alt='Icon'/></a>"
        },

        createCourseHeaderBlock: function (course) {
            return "<div class=\"side-block\">\n" +
                "        <div class=\"icon-btns\">" +
                "            <a href=\"" + course.link + "\"><i class='fa fa-arrow-right'></i></a>\n" +
                "        </div> " +
                "        <div>\n" +
                "            <div class=\"title-info\">\n" +
                "                " + gettext("Курс") + ": " + course.name +
                "            </div>\n" +
                "        </div>" +
                "   </div>"
        },

        createCourseInfoBlock: function (student, course) {
            let leftCourseBlock = ""
            if (course.membership_info.finished_date !== null) {
                leftCourseBlock = "<div class='single-info'>" +
                    "     <span class='has-text-danger'>" + gettext("Покинул курс") + " " + course.membership_info.finished_date + ". " + course.membership_info.left_reason + "</span>" +
                    "</div>"
            }

            let absences_count = 0
            if (course.hasOwnProperty("absences") && course.absences.hasOwnProperty(student.id)) {
                absences_count = course.absences[student.id]
            }

            let lessonsQuotaBlock = ""
            if (course.has_lessons_quota && course.lessons_quota !== null) {
                let edit_link = "/ajax/courses/" + course.id + "/students/" + student.id + "/quota/"
                let attended = course.lessons_quota.attended
                let quota = course.lessons_quota.quota
                lessonsQuotaBlock = "<div class=\"students-count-info\">" + gettext("Посещений") + ": " + attended + "/" + quota +
                    "        <div class=\"icon-inline-btn\">" +
                    "            <a href='#' class='ajax-form' data-modal='student-quota-form-modal' data-place='form-fragment' " +
                    "                   data-url=\"" + edit_link + "\"><i class='fa fa-pencil-alt'></i></a>\n" +
                    "        </div> " +
                    "</div> "
            }

            let progressBlock =
                "        <div class=\"single-info\">\n" +
                "            <progress class=\"months progress is-warning\" value=\"" + course.months_passed + "\" max=\"" + course.months + "\">30%</progress>\n" +
                "        </div>\n" +
                "        <div class=\"single-info\">\n" +
                interpolate(gettext("Пройдено %(passed)s месяцев из %(total)s"), {
                    passed: course.months_passed,
                    total: course.months,
                }, true) +
                "        </div>\n";
            if (course.status === "finished") {
                let endDate = ""
                if (course.end_date !== null) {
                    endDate = ": " + course.end_date;
                }
                progressBlock =
                    "<div class='single-info'>" +
                    "   <b>" + gettext("Курс завершен") + "</b>" + endDate +
                    "</div>"
            }
            return "<div class=\"side-block\">\n" +
                "        <div>\n" +
                "            <div class=\"schedule-info\">\n" +
                "                " + course.schedule +
                "            </div>\n" +
                "            <div class=\"students-count-info\">" + gettext("Пропусков") + ": " + absences_count + "</div>" +
                "        " + lessonsQuotaBlock +
                "            <div class=\"clearfix\"></div>\n" +
                "        </div>\n" +
                "        <div class=\"single-info\">\n" +
                "            " + gettext("Начало") + ": " + course.start_date + "\n" +
                "        </div>\n" +
                progressBlock +
                leftCourseBlock +
                "    </div>"
        },

        createPaymentBlock: function (course, student, payment_info) {
            let transactionsUrl = "/courses/" + course.id + "/transactions/?selected_student=" + student.id
            let addPaymentUrl = "/ajax/courses/" + course.id + "/students/" + student.id + "/payments/new/"
            let items = ""
            for (let i in payment_info.payment_months) {
                let month = payment_info.payment_months[i]
                if (month.need_to_pay) {
                    if (month.paid) {
                        items += "<div class=\"item paid\"></div>"
                    } else {
                        items += "<div class=\"item not-paid\"></div>"
                    }
                } else {
                    if (month.paid) {
                        items += "<div class=\"item paid\"></div>"
                    } else {
                        items += "<div class=\"item\"></div>"
                    }
                }
                if (i < payment_info.payment_months.length - 1) {
                    items += "<div class=\"divider\"></div>"
                }
            }

            return "<div class=\"side-block\">\n" +
                "        <div class=\"icon-btns\">" +
                "            <a href=\"" + transactionsUrl + "\"><i class='fas fa-list'></i></a>\n" +
                "            <a href='#' id='add_payment-link' data-url=\"" + addPaymentUrl + "\" class='ajax-form' data-place='form-fragment'" +
                "            data-modal='add-payment-form-modal'>" +
                "             <img src=\"/static/img/icons/ic_plus.svg\" alt=\"Add payment\"/></a>\n" +
                "        </div> " +
                "        <div class=\"title-info\">" + gettext("Оплата") + "</div>\n" +
                "        <div class=\"clearfix\"></div>\n" +
                "        <div class=\"payments-view\">\n" +
                "         " + items +
                "        </div>\n" +
                "        <div class=\"single-info\">\n" +
                interpolate(gettext("Оплачено %(paid)s месяцев из %(total)s"), {
                    paid: payment_info.paid_months_count,
                    total: payment_info.payable_months_count,
                }, true) +
                "        </div>\n" +
                "        <div class=\"single-info\">\n" +

                interpolate(gettext("Оплачено %(paid)s из %(total)s"), {
                    paid: payment_info.total_paid,
                    total: payment_info.total_price,
                }, true) +
                "        </div>\n" +
                "    </div>"
        },

        createContractBlock: function (course, student, contract_info) {
            if (contract_info != null) {
                let html = "<div class=\"side-block\">\n" +
                    "        <div style='position: relative;'>\n" +
                    "            <div class=\"title-info\">\n" +
                    "              " + gettext("Контракт") +
                    "            </div>\n"
                let buildContractUrl = "/ajax/courses/" + course.id + "/students/" + student.id + "/build-contract/"
                let buttonText = gettext("Открыть");
                if(contract_info.status === "not_created") {
                    buttonText = gettext("Составить")
                }
                html += "    <a  href='#' data-url=\"" + buildContractUrl + "\" class=\"crm-button button-info ajax-form\" data-place=\"form-fragment\" data-modal=\"build-contract-modal-form\">\n" +
                    "           " + buttonText +
                    "        </a>\n"
                html += "        <div class=\"clearfix\"></div>\n" +
                    "        </div>\n" +
                    "    </div>"
                return html
            }
            return ""
        },

        createInviteBlock: function (student) {
            let html = "<div class=\"side-block\">\n" +
                "        <div style='position: relative;'>\n" +
                "            <div class=\"title-info\">\n" +
                gettext("Аккаунт") +
                "            </div>\n"
            let inviteUrl = "/ajax/students/" + student.id + "/invite/"
            html += "    <a data-url=\"" + inviteUrl + "\" class=\"crm-button ajax-form button-info\" " +
                "               data-modal='invite-form-modal' data-place='form-fragment'>\n" +
                gettext("Пригласить") +
                "        </a>\n"

            html += "        <div class=\"clearfix\"></div>\n" +
                "        </div>\n" +
                "    </div>"
            return html
        },

        createStudentPasswordChangeBlock: function (student) {
            let html = "<div class=\"side-block\">\n" +
                "        <div style='position: relative;'>\n" +
                "            <div class=\"title-info\">\n" +
                gettext("Аккаунт") +
                "            </div>\n"
            let inviteUrl = "/ajax/students/" + student.id + "/change-password/"
            html += "    <a data-url=\"" + inviteUrl + "\" class=\"crm-button ajax-form button-info\" " +
                "               data-modal='change-password-form-modal' data-place='form-fragment'>\n" +
                gettext("Сбросить пароль") +
                "        </a>\n"

            html += "        <div class=\"clearfix\"></div>\n" +
                "        </div>\n" +
                "    </div>"
            return html
        },

        createRegisterBlock: function (student) {
            let html = "<div class=\"side-block\">\n" +
                "        <div style='position: relative;'>\n" +
                "            <div class=\"title-info\">\n" +
                gettext("Добавить в группу") +
                "            </div>\n"
            let registerUrl = "/ajax/students/" + student.id + "/register/"
            html += "    <a data-url=\"" + registerUrl + "\" class=\"crm-button ajax-form button-info\" " +
                "               data-modal='student-form-modal' data-place='form-fragment'>\n" +
                gettext("Добавить") +
                "        </a>\n"

            html += "        <div class=\"clearfix\"></div>\n" +
                "        </div>\n" +
                "    </div>"
            return html
        },

        createActionBlock: function (action) {
            let html = "<div class=\"side-block\">\n" +
                "        <div style='position: relative;'>\n" +
                "            <div class=\"title-info\">\n" +
                "               " + action.title +
                "            </div>\n " +
                "        <a href=\"" + action.url + "\" class=\"crm-button button-info " + action.css_class + " \">\n" +
                "           " + action.button +
                "        </a>\n"

            html += "        <div class=\"clearfix\"></div>\n" +
                "        </div>\n" +
                "    </div>"
            return html
        }
    },

    loadStudentInfo: function (id) {
        Students.showPanel()
        Students.showProgress()

        $.get("/old_api/students/" + id + "/", function (student) {
            Students.showPanel()
            Students.showStudentInfo(student)
            Students.updateStudentOnPage(student)
        }).fail(function () {
            Students.hidePanel()
        })
    }
}



