import hashlib
from urllib.parse import urlencode

from django import template
from django.http import QueryDict

register = template.Library()

PAGE, PER_PAGE = 1, 20


@register.filter
def gravatar(email, size=128):
    if email:
        return "https://www.gravatar.com/avatar/%s?%s" % (hashlib.md5(email.lower().encode("utf-8")).hexdigest(),
                                                          urlencode({'s': str(size), 'd': 'robohash'}))
    return f"https://www.gravatar.com/avatar/asdjhasdjahsdjhasdjk?s={size}&d=robohash"


@register.filter
def get(_dict, key):
    return _dict.get(key) if isinstance(_dict, dict) else None


@register.filter
def multiply(one, another):
    return one * another


@register.filter
def get_or_none(collection, key):
    if type(collection) == dict and key in collection:
        return collection[key]
    if type(collection) == list and key < len(collection):
        return collection[key]
    return None


@register.filter(name="range")
def _range(number, end=None):
    if end:
        return range(number, end)
    return range(number)


@register.filter
def to_int(text):
    return int(text)


@register.filter
def leading_zero(number):
    return "{:02d}".format(number)


@register.inclusion_tag("blocks/pagination_pages.html")
def pages(queryset, request, extra_params=None):
    page = int(request.GET.get("page", PAGE)) - 1
    n = int(request.GET.get("per_page", PER_PAGE))
    count = queryset.count()
    pages_count = int(count / n)
    if count % n > 0:
        pages_count += 1

    context = {
        'pages': pagination(pages_count, page + 1),
        'per_page': n,
        'current_page': page + 1,
        'last': pages_count,
    }

    if isinstance(extra_params, QueryDict):
        extra_params = extra_params.copy()

        if "per_page" in extra_params:
            extra_params.pop("per_page")
        if "page" in extra_params:
            extra_params.pop("page")

        context["extra_params"] = extra_params.urlencode()
    elif isinstance(extra_params, dict):
        params = {}
        for key, value in extra_params.items():
            params[key] = value or ''

        context["extra_params"] = urlencode(params)
    return context


def pagination(total, page=1, neighbour_count=2):
    result = []
    if page - neighbour_count > 2:
        result.append('first')
    if page - neighbour_count == 2:
        result.append(1)
    for i in range(page - neighbour_count, page + neighbour_count + 1):
        if 0 < i <= total:
            result.append(i)
    if page + neighbour_count < total - 1:
        result.append('last')
    if page + neighbour_count == total - 1:
        result.append(total)
    return result


@register.filter
def paginate(queryset, request):
    if not queryset:
        return []

    page = int(request.GET.get("page", PAGE)) - 1
    n = int(request.GET.get("per_page", PER_PAGE))

    return queryset[page * n:page * n + n]


@register.filter
def counter(request, counter):
    page = int(request.GET.get("page", PAGE)) - 1
    n = int(request.GET.get("per_page", PER_PAGE))
    return page * n + counter


@register.filter
def thousands(number):
    return "{:,.2f}".format(number).replace(",", " ")
