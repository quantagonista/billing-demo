import uuid

from django.contrib import admin, messages
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.hashers import make_password
from django.db.models import Exists, OuterRef, BooleanField
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _
from polymorphic.admin import (
    StackedPolymorphicInline, PolymorphicInlineSupportMixin, PolymorphicParentModelAdmin,
    PolymorphicChildModelFilter, PolymorphicChildModelAdmin
)

from . import models, forms, services
from .utils import make_username


@admin.register(models.CustomUser)
class CustomUserAdmin(UserAdmin):
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'password1', 'password2'),
        }),
        ("Customer", {
            'classes': ('wide',),
            'fields': ('customer',),
        }),
    )
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions'),
        }),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
        ("Customer", {
            'classes': ('wide',),
            'fields': ('customer',),
        }),
    )


@admin.register(models.ExternalCrmIntegration)
class ExternalCrmIntegrationAdmin(admin.ModelAdmin):
    list_display = ["user", "created_at", "value", "crm_url"]
    readonly_fields = ["value"]


@admin.register(models.CustomerType)
class CustomerTypeAdmin(admin.ModelAdmin):
    pass


class CustomerLicenseInline(admin.StackedInline):
    model = models.License
    extra = 1
    formset = forms.LicenseAdminFormSet


class CustomerInvoiceInline(admin.StackedInline):
    model = models.Invoice
    extra = 1
    form = forms.InvoiceAdminForm


class CustomerTransactionsInline(admin.StackedInline):
    model = models.Transaction
    extra = 1
    form = forms.TransactionAdminForm

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(models.Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'type', 'url',
        'users_count', 'contact_phone_number',
        'has_account', 'server', 'org_id'
    )

    fields = ('name', 'description', 'type', 'url',
              'users_count', 'contact_person', 'contact_phone_number',
              'contact_email', 'contact_skype', 'assignees', 'created_by',
              'server', 'org_id', 'crm_status',
              )

    search_fields = (
        'name', 'contact_person', 'contact_phone_number',
        'contact_email', 'contact_skype', 'users_count'
    )

    list_filter = ('type', 'assignees', 'created_by', 'server')

    inlines = (
        CustomerLicenseInline,
        CustomerInvoiceInline,
        CustomerTransactionsInline,
    )

    actions = ['add_account']

    def add_account(self, request, customers):
        sep = '<br/>'

        message_title = _('Учетные данные <имя-пользователя> : <пароль>')
        message = message_title + sep
        existing_accounts = {user.customer for user in models.CustomUser.objects.filter(customer__in=customers)}

        accounts = []

        for customer in customers:
            if customer in existing_accounts:
                continue

            username = make_username(customer.contact_person)
            password = str(uuid.uuid4())

            accounts.append(
                models.CustomUser(
                    username=username,
                    email=customer.contact_email,
                    password=make_password(password),
                    customer=customer,
                    is_active=True,
                    is_partner_account=True,
                )
            )
            message += f"{username} : {password}{sep}"

        if accounts:
            models.CustomUser.objects.bulk_create(accounts)
            messages.success(request, mark_safe(message))

    add_account.short_description = _('Создать аккаунт')

    def has_account(self, customer):
        return customer.has_account if hasattr(customer, 'has_account') else None

    has_account.short_description = _('Есть аккаунт?')
    has_account.boolean = True

    def get_queryset(self, request):
        has_account = Exists(models.CustomUser.objects.filter(customer=OuterRef('id')), output_field=BooleanField())
        return models.Customer.objects.all().annotate(has_account=has_account)


@admin.register(models.LicenseEntity)
class LicenseEntityAdmin(admin.ModelAdmin):
    search_fields = ('name',)


@admin.register(models.License)
class LicenseAdmin(admin.ModelAdmin):
    list_display = (
        'customer', 'license_entity', 'type',
        'start_date', 'end_date', 'active',
        'price', 'discount_percentage'
    )
    list_filter = (
        'customer', 'license_entity', 'type',
        'start_date', 'end_date', 'active'
    )

    form = forms.LicenseAdminForm


class InvoiceItemInline(StackedPolymorphicInline):
    class LicenseInvoiceItemInline(StackedPolymorphicInline.Child):
        model = models.LicenseInvoiceItem
        form = forms.LicenseInvoiceItemAdminForm

    class ChildInvoiceItemInline(StackedPolymorphicInline.Child):
        model = models.InvoiceItem
        form = forms.InvoiceItemAdminForm

    model = models.InvoiceItem
    form = forms.InvoiceItemAdminForm
    child_inlines = (LicenseInvoiceItemInline, ChildInvoiceItemInline)


@admin.register(models.Invoice)
class InvoiceAdmin(PolymorphicInlineSupportMixin, admin.ModelAdmin):
    list_display = (
        'customer', 'title', 'initial_amount', 'discount_amount',
        'final_amount', 'is_paid', 'payment_date', 'last_notified_at'
    )
    list_filter = ('customer', 'is_paid')
    readonly_fields = ('last_notified_at',)

    inlines = (InvoiceItemInline,)
    form = forms.InvoiceAdminForm

    def has_change_permission(self, request, obj=None):
        return False if obj and obj.is_paid else super().has_change_permission(request, obj)


@admin.register(models.LicenseInvoiceItem)
class LicenseInvoiceItemAdmin(PolymorphicChildModelAdmin):
    form = forms.LicenseInvoiceItemAdminForm


@admin.register(models.InvoiceItem)
class InvoiceItemAdmin(PolymorphicParentModelAdmin):
    base_model = models.InvoiceItem
    child_models = (models.LicenseInvoiceItem, models.InvoiceItem)
    list_filter = (PolymorphicChildModelFilter,)
    form = forms.InvoiceItemAdminForm
    list_display = (
        'title', 'invoice', 'initial_amount',
        'discount_amount', 'final_amount'
    )


@admin.register(models.PaymentMethod)
class PaymentMethodAdmin(admin.ModelAdmin):
    list_display = ('name', 'is_active')


@admin.register(models.Transaction)
class TransactionAdmin(admin.ModelAdmin):
    list_display = (
        'customer', 'payment_method', 'invoice',
        'type', 'amount', 'is_canceled'
    )
    list_filter = ('payment_method', 'customer', 'is_canceled')

    form = forms.TransactionAdminForm

    actions = ['cancel_action']

    def cancel_action(self, request, queryset):
        services.TransactionService.cancel_transactions(transactions=queryset)

    cancel_action.short_description = _('Аннулировать выбранные транзакции')

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(models.CrmServer)
class CrmServerAdmin(admin.ModelAdmin):
    list_display = ('name', 'url')
