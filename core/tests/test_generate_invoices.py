import random

from django.test import TestCase
from django.utils.timezone import now, timedelta as tdelta, datetime as dtime
from dateutil.relativedelta import relativedelta as rdelta

from core.choices import LicenseTypes
from core.models import Customer, License, LicenseEntity, LicenseInvoiceItem, Invoice
from core.models.choices import CustomerStatuses
from core.services import InvoiceService


class GenerateInvoiceTestCase(TestCase):
    @staticmethod
    def create_customer(status):
        return Customer.objects.create(name=f'Org-{random.randint(0, 9999)}', users_count=0, status=status)

    def create_ending_license(self,
                              end_date: dtime,
                              customer_status=CustomerStatuses.ACTIVE,
                              license_active=True,
                              type_=LicenseTypes.MONTHLY,
                              length_in_days=30, price=29):
        license_ = License.objects.create(
            end_date=end_date, start_date=end_date - tdelta(days=length_in_days),
            customer=self.create_customer(customer_status),
            license_entity=LicenseEntity.get_default(),
            price=price, type=type_, active=license_active
        )

        return license_

    def create_next_invoice_for(self, _license: License, is_paid=False):
        license_delta = _license.end_date - _license.start_date

        invoice = Invoice.objects.create(customer=_license.customer, title='Invoice', is_paid=is_paid)
        invoice_item = LicenseInvoiceItem.objects.create(
            license=_license, invoice=invoice, title=invoice.title,
            initial_amount=_license.price, discount_amount=0,
            start_date=_license.end_date, end_date=_license.end_date + license_delta
        )

        return invoice, invoice_item

    def get_invoices_count_for_license(self, license: License):
        return LicenseInvoiceItem.objects.filter(license=license)

    # No Invoice after License's end - START #

    def test_it_does_not_generates_invoice_if_more_than_week_until_license_ends(self):
        more_than_week_from_now = now() + tdelta(days=9)
        license = self.create_ending_license(end_date=more_than_week_from_now)

        InvoiceService.generate_invoices_for_ending_licences()

        invoices = self.get_invoices_count_for_license(license)

        self.assertEqual(invoices.count(), 0)

    def test_it_generates_invoice_if_less_than_week_until_license_ends(self):
        less_than_week_from_now = now().date() + tdelta(days=5)
        license = self.create_ending_license(end_date=less_than_week_from_now)

        InvoiceService.generate_invoices_for_ending_licences()

        invoices = self.get_invoices_count_for_license(license)

        self.assertEqual(invoices.count(), 1)
        self.assertEqual(invoices[0].start_date, license.end_date)
        self.assertEqual(invoices[0].end_date, license.end_date + rdelta(months=1))

    def test_it_generate_invoice_only_for_active_licenses(self):
        less_than_week_from_now = now().date() + tdelta(days=5)
        active_license = self.create_ending_license(end_date=less_than_week_from_now, license_active=True)
        inactive_license = self.create_ending_license(end_date=less_than_week_from_now, license_active=False)

        InvoiceService.generate_invoices_for_ending_licences()

        active_invoices = self.get_invoices_count_for_license(active_license)
        inactive_invoices = self.get_invoices_count_for_license(inactive_license)

        self.assertEqual(active_invoices.count(), 1)
        self.assertEqual(inactive_invoices.count(), 0)

    def test_it_generate_invoice_only_for_active_customer(self):
        less_than_week_from_now = now().date() + tdelta(days=5)
        active_customer_license = self.create_ending_license(end_date=less_than_week_from_now, customer_status=CustomerStatuses.ACTIVE)
        inactive_customer_license = self.create_ending_license(end_date=less_than_week_from_now, customer_status=CustomerStatuses.INACTIVE)
        test_drive_customer_license = self.create_ending_license(end_date=less_than_week_from_now,
                                                                 customer_status=CustomerStatuses.TEST_DRIVE)

        InvoiceService.generate_invoices_for_ending_licences()

        active_customer_invoices = self.get_invoices_count_for_license(active_customer_license)
        inactive_customer_invoices = self.get_invoices_count_for_license(inactive_customer_license)
        test_drive_customer_invoices = self.get_invoices_count_for_license(test_drive_customer_license)

        self.assertEqual(active_customer_invoices.count(), 1)
        self.assertEqual(inactive_customer_invoices.count(), 0)
        self.assertEqual(test_drive_customer_invoices.count(), 0)

    def test_it_generates_1_invoice_if_called_twice(self):
        today = now()
        tomorrow = today + tdelta(days=1)

        less_than_week_from_now = now().date() + tdelta(days=5)
        license = self.create_ending_license(end_date=less_than_week_from_now)

        InvoiceService.generate_invoices_for_ending_licences(today)
        InvoiceService.generate_invoices_for_ending_licences(tomorrow)

        invoices = self.get_invoices_count_for_license(license)

        self.assertEqual(invoices.count(), 1)

    # 1 unpaid Invoice after License's end - START #

    def test_it_does_not_make_unpaid_invoice_period_longer_if_payment_period_is_not_passing(self):
        end_date_which_should_not_trigger_updates = now().date() - (rdelta(months=1) - rdelta(days=15))
        license = self.create_ending_license(end_date=end_date_which_should_not_trigger_updates)
        _, invoice_item = self.create_next_invoice_for(license, is_paid=False)

        invoice_start_before, invoice_end_before = invoice_item.start_date, invoice_item.end_date

        InvoiceService.generate_invoices_for_ending_licences()

        self.assertEqual(self.get_invoices_count_for_license(license).count(), 1)

        invoice_item.refresh_from_db()
        self.assertEqual(invoice_item.start_date, invoice_start_before)
        self.assertEqual(invoice_item.end_date, invoice_end_before)

    def test_it_makes_unpaid_invoice_period_longer_if_payment_period_is_passing(self):
        end_date_which_should_trigger_updates = now().date() - (rdelta(months=1) - rdelta(days=3))
        license = self.create_ending_license(end_date=end_date_which_should_trigger_updates)
        _, invoice_item = self.create_next_invoice_for(license, is_paid=False)

        invoice_start_before, invoice_end_before = invoice_item.start_date, invoice_item.end_date

        InvoiceService.generate_invoices_for_ending_licences()

        self.assertEqual(self.get_invoices_count_for_license(license).count(), 1)

        invoice_item.refresh_from_db()
        self.assertEqual(invoice_item.start_date, invoice_start_before)
        self.assertEqual(invoice_item.end_date, invoice_end_before + rdelta(months=1))

    def test_it_makes_unpaid_invoice_price_higher_if_payment_period_is_passing(self):
        end_date_which_should_trigger_updates = now().date() - (rdelta(months=1) - rdelta(days=3))
        license = self.create_ending_license(price=29, end_date=end_date_which_should_trigger_updates)
        invoice, invoice_item = self.create_next_invoice_for(license, is_paid=False)

        InvoiceService.generate_invoices_for_ending_licences()

        self.assertEqual(self.get_invoices_count_for_license(license).count(), 1)

        invoice.refresh_from_db()
        self.assertEqual(invoice.final_amount, 58)

    def test_it_does_not_updates_invoice_if_called_twice(self):
        today = now()
        tomorrow = today + tdelta(days=1)

        end_date_which_should_not_trigger_updates = today.date() - (rdelta(months=1) - rdelta(days=15))
        license = self.create_ending_license(end_date=end_date_which_should_not_trigger_updates)

        _, invoice_item = self.create_next_invoice_for(license, is_paid=False)

        invoice_start_before, invoice_end_before = invoice_item.start_date, invoice_item.end_date

        InvoiceService.generate_invoices_for_ending_licences(today)
        InvoiceService.generate_invoices_for_ending_licences(tomorrow)

        self.assertEqual(self.get_invoices_count_for_license(license).count(), 1)

        invoice_item.refresh_from_db()

        self.assertEqual(invoice_item.start_date, invoice_start_before)
        self.assertEqual(invoice_item.end_date, invoice_end_before)

    def test_it_does_not_updates_invoice_if_invoice_is_paid(self):
        end_date_which_should_not_trigger_updates = now().date() - (rdelta(months=1) - rdelta(days=15))
        license = self.create_ending_license(end_date=end_date_which_should_not_trigger_updates)

        _, invoice_item = self.create_next_invoice_for(license, is_paid=True)

        invoice_start_before, invoice_end_before = invoice_item.start_date, invoice_item.end_date

        InvoiceService.generate_invoices_for_ending_licences()

        self.assertEqual(self.get_invoices_count_for_license(license).count(), 1)

        invoice_item.refresh_from_db()

        self.assertEqual(invoice_item.start_date, invoice_start_before)
        self.assertEqual(invoice_item.end_date, invoice_end_before)
