from datetime import date, timedelta
from random import randint

from django.test import TestCase
from django.utils import timezone

from core.choices import LicenseTypes
from core.models import Customer, License, LicenseEntity
from core.models.choices import CustomerStatuses
from core.services.license import LicenseCheckService


class CustomerLicenseEndDateTestCase(TestCase):
    def create_customer(self, ):
        return Customer.objects.create(
            name=f"Org {randint(1, 1000)}",
            users_count=100,
            status=CustomerStatuses.ACTIVE
        )

    def create_license_for(self, customer, start_date, end_date, active=True):
        entity, _ = LicenseEntity.objects.get_or_create(name="CRM")

        return License.objects.create(
            customer=customer,
            license_entity=entity,
            type=LicenseTypes.MONTHLY,
            price=100,
            start_date=start_date,
            end_date=end_date,
            active=active
        )

    def test_customer_without_licenses_has_null_license_end_date(self):
        customer = self.create_customer()

        end_date = LicenseCheckService.get_customer_license_end_date(customer)

        self.assertIsNone(end_date)

    def test_customer_with_single_license_in_the_past(self):
        customer = self.create_customer()
        self.create_license_for(customer, start_date=date(2021, 1, 1), end_date=date(2021, 1, 2))

        end_date = LicenseCheckService.get_customer_license_end_date(customer)

        self.assertEqual(end_date, date(2021, 1, 2))

    def test_customer_with_two_license_in_the_past(self):
        customer = self.create_customer()
        self.create_license_for(customer, start_date=date(2021, 1, 1), end_date=date(2021, 1, 2))
        self.create_license_for(customer, start_date=date(2021, 2, 2), end_date=date(2021, 2, 3))

        end_date = LicenseCheckService.get_customer_license_end_date(customer)

        self.assertEqual(end_date, date(2021, 2, 3))

    def test_customer_with_two_license_in_the_past_not_depends_on_creation_order(self):
        customer = self.create_customer()
        self.create_license_for(customer, start_date=date(2021, 2, 2), end_date=date(2021, 2, 3))
        self.create_license_for(customer, start_date=date(2021, 1, 1), end_date=date(2021, 1, 2))

        end_date = LicenseCheckService.get_customer_license_end_date(customer)

        self.assertEqual(end_date, date(2021, 2, 3))

    def test_inactive_licenses_are_not_counted(self):
        customer = self.create_customer()
        self.create_license_for(customer, start_date=date(2021, 1, 1), end_date=date(2021, 1, 2))
        self.create_license_for(customer, start_date=date(2021, 2, 2), end_date=date(2021, 2, 3), active=False)

        end_date = LicenseCheckService.get_customer_license_end_date(customer)

        self.assertEqual(end_date, date(2021, 1, 2))

    def test_customer_with_active_license_for_today(self):
        today = timezone.now().date()

        customer = self.create_customer()
        self.create_license_for(customer, start_date=date(2021, 2, 2), end_date=date(2021, 2, 3))
        self.create_license_for(customer, start_date=date(2021, 1, 1), end_date=date(2021, 1, 2))
        self.create_license_for(customer, start_date=today - timedelta(10), end_date=today + timedelta(10))

        end_date = LicenseCheckService.get_customer_license_end_date(customer)

        self.assertEqual(end_date, today + timedelta(10))

    def test_licenses_in_the_future_are_not_counted(self):
        today = timezone.now().date()

        customer = self.create_customer()
        self.create_license_for(customer, start_date=date(2021, 2, 2), end_date=date(2021, 2, 3))
        self.create_license_for(customer, start_date=date(2021, 1, 1), end_date=date(2021, 1, 2))
        self.create_license_for(customer, start_date=today + timedelta(10), end_date=today + timedelta(20))

        end_date = LicenseCheckService.get_customer_license_end_date(customer)

        self.assertEqual(end_date, date(2021, 2, 3))

    def test_licenses_in_the_future_are_not_counted_twice(self):
        today = timezone.now().date()

        customer = self.create_customer()
        self.create_license_for(customer, start_date=today + timedelta(10), end_date=today + timedelta(20))

        end_date = LicenseCheckService.get_customer_license_end_date(customer)

        self.assertIsNone(end_date)

    def test_licenses_started_from_today_are_counted(self):
        today = timezone.now().date()

        customer = self.create_customer()
        self.create_license_for(customer, start_date=today, end_date=today + timedelta(20))

        end_date = LicenseCheckService.get_customer_license_end_date(customer)

        self.assertEqual(end_date, today + timedelta(20))
