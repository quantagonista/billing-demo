from django.utils.translation import gettext_lazy as _

SOMETHING_WENT_WRONG = _('Что-то пошло не так')

# Licenses
LICENSE_PERIOD_INTERSECTION_ERROR = _('Период данной лицензии пересекается с другими лицензиями')
INVALID_DISCOUNT_AMOUNT_ERROR = _('Сумма скидки превышает стоимость')
INVALID_LICENSE_DATE_ERROR = _('Дата не может быть меньше предыдущей')

# Transactions
INVALID_PAYMENT_DATE_ERROR = _('Если Счет оплачен, Дата оплаты обязательна')
INVALID_CREDIT_TRANSACTION_AMOUNT_ERROR = _('На счету недостаточно средств')
INVALID_TRANSACTION_FOR_PAID_INVOICE = _('Невозможно создать транзацию для оплаченного Счета')
INVALID_CREDIT_TRANSACTION_FOR_INVOICE = _('Сумма транзакции не соответствует сумме Счета')
INVALID_INVOICE_FOR_CREDIT_TRANSACTION = _('Транзакция не привязана к Счету')
INVALID_DEBIT_TRANSACTION = _('Неверная транзакция пополнения')
INVALID_INVOICE_FOR_DEBIT_TRANSACTION = _('Транзакция пополнения не должна быть привязана к Счету')

# CRM Integrations
CRM_INTEGRATION_BASE_ERROR = _('Не удалось произвести интеграцию')
CRM_SERVER_UNAVAILABLE_ERROR = _('Сервер CRM системы недоступен')
CRM_DEFAULT_SERVER_ERROR = _('Не настроен сервер по умолчанию')
CRM_REGISTRATION_ERROR = _('Ошибка регистрации организации')
CRM_INTEGRATION_INCONSISTENT_ERROR = _('Проверьте данные интеграции URL не совпадают')
CRM_INVALID_STATUS = _('Некорректный статус для CRM')
CRM_STATUS_CHANGE_SUCCESS = _('CRM статус успешно изменен')
INVALID_CUSTOMER_BY_TOKEN = _('Пользователь не привязан к клиенту')
INVALID_CUSTOMER_ORG_ID = _('ID организаций не совпадают')

# Payments
INIT_PAYMENT_ERROR = _('Ошибка платежной системы. Попробуйте другой способ оплаты')
INVALID_PAYMENT_PROVIDER_ERROR = _('Неверная платежная система')

# Customers
CUSTOMER_CREATE_SUCCESS = _('Клиент успешно создан')
CUSTOMER_MULTIPLE_ACCOUNTS = _('Клиент имеет несколько аккаунтов. Невозможно удалить интеграцию')
CUSTOMER_HAS_NO_SERVER = _('Клиент не подключен к серверу CRM. Выберите сервер и попробуйте снова')
CUSTOMER_HAS_NO_INTEGRATION = _('Клиент не имеет интеграции с сервером')
CUSTOMER_BY_EMAIL_EXISTS = _('Клиент с таким email уже есть')
CUSTOMER_BY_NAME_EXISTS = _('Клиент с таким названием уже есть')
INVALID_CUSTOMER_DATA = _('Неверные данные клиента')
INVALID_CUSTOMER_FULL_NAME = _('Имя и фамилия должны быть разделены пробелом')
INVALID_CUSTOMER_ORG_TYPE = _('Неверный тип организации')


# Labels
EMAIL = 'Email'

START_DATE = _('Дата начала')
END_DATE = _('Дата конца')
PRICE = _('Стоимость')

CUSTOMER_NAME = _('Название компании')
