from django.utils.text import slugify


def make_username(string: str):
    mapper = {
        'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd',
        'е': 'e', 'ё': 'e', 'ж': 'zh', 'з': 'z', 'и': 'i',
        'й': 'i', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n',
        'о': 'o', 'п': 'p', 'р': 'r', 'с': 's', 'т': 't',
        'у': 'u', 'ф': 'f', 'х': 'h', 'ц': 'ts', 'ч': 'ch',
        'ш': 'sh', 'щ': 'sh', 'ь': '', 'ъ': '', 'ы': 'y',
        'э': 'e', 'ю': 'yu', 'я': 'ya',
    }

    def map_char(char: str):
        return mapper.get(char, char)

    return slugify(''.join([map_char(char) for char in str(string).lower()]))
