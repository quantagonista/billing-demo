from django.db import transaction
from django.db.models import QuerySet

from .. import models


class TransactionService:
    model = models.Transaction

    @classmethod
    @transaction.atomic
    def cancel_transactions(cls, transactions: QuerySet):
        models.Invoice.objects.filter(transaction__in=transactions).update(is_paid=False, payment_date=None)
        transactions.update(is_canceled=True, invoice=None)

    @classmethod
    def filter(cls, customer=None, type=None, start_date=None, end_date=None):
        transactions = cls.model.objects.all()

        if customer:
            transactions = transactions.filter(customer=customer)

        if type:
            transactions = transactions.filter(type=type)

        if start_date:
            transactions = transactions.filter(created_at__gte=start_date)

        if end_date:
            transactions = transactions.filter(created_at__lte=end_date)

        return transactions.order_by('-created_at')
