from datetime import timedelta
from decimal import Decimal
from typing import List, Tuple, Set, Dict

from django.utils import timezone

from core.models import Transaction, LicenseInvoiceItem, Customer


class MRRService:
    @classmethod
    def _get_next_month_s_start(cls, month_start):
        return (month_start + timedelta(32)).replace(day=1)

    @classmethod
    def get_transactions_by_months(cls, for_last_months=23):
        period_start = (timezone.now().date() - timedelta(for_last_months * 30)).replace(day=1)

        transactions = (Transaction
                        .objects
                        .filter(is_canceled=False, type=Transaction.DEBIT, created_at__gte=period_start)
                        .order_by("created_at"))

        amount_by_period = {}
        for transaction in transactions:
            month = transaction.created_at.date().replace(day=1)
            if month not in amount_by_period:
                amount_by_period[month] = Decimal('0')

            amount_by_period[month] += transaction.amount

        return amount_by_period

    @classmethod
    def get_paid_for_months(cls, for_last_months=23):
        period_start = (timezone.now().date() - timedelta(for_last_months * 30)).replace(day=1)

        paid_invoice_items: List[LicenseInvoiceItem] = LicenseInvoiceItem.objects.filter(invoice__is_paid=True, end_date__gt=period_start)

        amount_by_period = {}
        while period_start < timezone.now().date():
            amount_by_period[period_start] = Decimal(0)
            period_start = cls._get_next_month_s_start(period_start)

        for invoice_item in paid_invoice_items:
            paid_days_count = (invoice_item.end_date - invoice_item.start_date).days

            try:
                price_for_one_day = invoice_item.final_amount / Decimal(str(paid_days_count))
            except:
                price_for_one_day = 0

            for i in range(paid_days_count):
                month = (invoice_item.start_date + timedelta(i)).replace(day=1)
                if month in amount_by_period:
                    amount_by_period[month] += price_for_one_day

        return amount_by_period

    @classmethod
    def get_monthly_payment_by_customers(cls, for_last_months=6) -> Tuple[Set[Customer], Dict]:
        period_start = (timezone.now().date() - timedelta(for_last_months * 30)).replace(day=1)

        paid_invoice_items = LicenseInvoiceItem.objects.filter(invoice__is_paid=True, end_date__gt=period_start)
        last_paid_invoice = paid_invoice_items.order_by("-end_date").first()
        period_end = last_paid_invoice.end_date if last_paid_invoice else timezone.now().date()

        customers = set()
        amount_by_period = {}
        while period_start < period_end:
            amount_by_period[period_start] = {
                "total": Decimal(0)
            }
            period_start = cls._get_next_month_s_start(period_start)

        for invoice_item in paid_invoice_items:
            customer = invoice_item.invoice.customer
            customers.add(customer)

            paid_days_count = (invoice_item.end_date - invoice_item.start_date).days

            try:
                price_for_one_day = invoice_item.final_amount / Decimal(str(paid_days_count))
            except:
                price_for_one_day = 0

            for i in range(paid_days_count):
                month = (invoice_item.start_date + timedelta(i)).replace(day=1)
                if month in amount_by_period:
                    if customer.id not in amount_by_period[month]:
                        amount_by_period[month][customer.id] = Decimal(0)
                    amount_by_period[month][customer.id] += price_for_one_day
                    amount_by_period[month]["total"] += price_for_one_day

        return customers, amount_by_period
