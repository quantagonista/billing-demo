import datetime
from typing import Dict

from django.db.models import QuerySet, OuterRef, DateField, Subquery
from django.urls import reverse
from django.utils import timezone

from core.models import License
from core.models.choices import CustomerStatuses
from core.services import CustomerService


class DashboardService:
    @classmethod
    def get_dashboard_info(cls):
        customers = CustomerService.get_filtered_customers().only('pk', 'name')
        customers = customers.annotate(next_payment_date=cls._get_customer_next_payment_date()).order_by('next_payment_date')
        return cls._split_customers_by_columns(customers)

    @classmethod
    def _split_customers_by_columns(cls, customers: QuerySet) -> Dict:
        test_drive_customers = {'count': 0, 'list': []}
        inactive_customers = {'count': 0, 'list': []}
        paid_customers = {'count': 0, 'list': []}
        need_to_pay_customers = {'count': 0, 'list': []}
        late_customers = {'count': 0, 'list': []}

        for customer in customers:
            days_until_next_payment = cls._get_days_until_next_payment(customer.next_payment_date)
            customer.days_until_next_payment = days_until_next_payment

            if customer.status == CustomerStatuses.TEST_DRIVE:
                test_drive_customers['list'].append(customer)
                test_drive_customers['count'] += 1

            elif customer.status == CustomerStatuses.INACTIVE:
                inactive_customers['list'].append(customer)
                inactive_customers['count'] += 1

            elif days_until_next_payment > 10:
                paid_customers['list'].append(customer)
                paid_customers['count'] += 1

            elif days_until_next_payment > 0:
                need_to_pay_customers['list'].append(customer)
                need_to_pay_customers['count'] += 1

            else:
                late_customers['list'].append(customer)
                late_customers['count'] += 1

        base_url = reverse('customer-list')

        test_drive_customers['url'] = f"{base_url}?statuses={CustomerStatuses.TEST_DRIVE}"
        inactive_customers['url'] = f"{base_url}?statuses={CustomerStatuses.INACTIVE}"
        need_to_pay_customers['url'] = cls._append_id_query_params_to_url(base_url, need_to_pay_customers['list'])
        paid_customers['url'] = cls._append_id_query_params_to_url(base_url, paid_customers['list'])
        late_customers['url'] = cls._append_id_query_params_to_url(base_url, late_customers['list'])

        return {
            'test_drive_customers': test_drive_customers,
            'paid_customers': paid_customers,
            'need_to_pay_customers': need_to_pay_customers,
            'late_customers': late_customers,
            'inactive_customers': inactive_customers,
        }

    @classmethod
    def _get_customer_next_payment_date(cls):
        """ returns Subquery which gets end_date of the last Customer's License """

        last_license = (License.objects
                        .filter(customer=OuterRef('pk'), active=True)
                        .order_by('-end_date')
                        .values_list('end_date', flat=True))[:1]

        return Subquery(last_license, output_field=DateField())

    @classmethod
    def _get_days_until_next_payment(cls, next_date: datetime.date = None):
        if not next_date:
            return 0

        return -(timezone.now().date() - next_date).days

    @classmethod
    def _append_id_query_params_to_url(cls, base_url, items: list):
        """ Returns `/base-url?id=1&id=2&...&id=n` """
        if not items:
            return base_url

        return f"{base_url}?id={items[0].id}{'&id='.join(map(lambda item: str(item.id), items[1:]))}"
