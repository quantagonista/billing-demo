from django.db import transaction
from django.db.models import Q

from core import models
from core.models import CustomerType, CustomUser, Customer, LicenseInvoiceItem, InvoiceItem, Transaction, License, Invoice
from core.models.choices import CustomerStatuses
from payments.models import PaymentOrder


class CustomerService:
    model = models.Customer

    @classmethod
    def get_filtered_customers(cls, types: list = None, statuses: list = None, search: str = None, ids: list = None):
        customers = cls.model.objects.all()

        if ids:
            customers = customers.filter(id__in=ids)

        if types:
            customers = customers.filter(type__in=types)

        if statuses:
            customers = customers.filter(status__in=statuses)

        if search:
            search_query = (
                Q(name__icontains=search) |
                Q(contact_phone_number__icontains=search) |
                Q(contact_email__icontains=search)
            )
            customers = customers.filter(search_query)

        return customers.order_by("-id")

    @classmethod
    def get_customer_types_as_choices(cls):
        return models.CustomerType.objects.all()

    @classmethod
    @transaction.atomic
    def create_customer(cls,
                        name: str,
                        type: CustomerType = None,
                        users_count: int = 1,
                        created_by: CustomUser = None,
                        url: str = None,
                        contact_phone_number: str = None,
                        contact_person: str = None,
                        contact_skype: str = None,
                        contact_email: str = None,
                        status: str = CustomerStatuses.ACTIVE) -> Customer:

        customer = Customer.objects.create(
            name=name, type=type, users_count=users_count, url=url, status=status,
            contact_phone_number=contact_phone_number,
            contact_person=contact_person,
            contact_skype=contact_skype,
            contact_email=contact_email,
            created_by=created_by
        )
        return customer

    @classmethod
    @transaction.atomic
    def delete_customer(cls, customer: Customer):
        LicenseInvoiceItem.objects.filter(license__customer=customer).delete()
        PaymentOrder.objects.filter(customer=customer).delete()
        InvoiceItem.objects.filter(invoice__customer=customer).delete()
        Transaction.objects.filter(customer=customer).delete()
        License.objects.filter(customer=customer).delete()
        Invoice.objects.filter(customer=customer).delete()
        customer.delete()
