from django.db import transaction as db_transaction
from django.utils import timezone

from core.choices import LicenseTypes
from core.constants import CRM_DEFAULT_SERVER_ERROR
from core.exceptions import CRMIntegrationError
from core.models import CrmServer, Customer, License, LicenseEntity
from core.models.choices import CustomerStatuses, CrmStatuses
from core.services import CrmIntegrationService


class TestDriveFacadeService:
    @classmethod
    @db_transaction.atomic
    def perform_test_drive_integration(cls, name: str, type: str, currency, language, full_name, email, phone):
        server = CrmServer.objects.filter(is_default=True).first()
        if not server:
            raise CRMIntegrationError(CRM_DEFAULT_SERVER_ERROR)

        customer = cls.create_customer_with_test_period_licence(name, type, full_name, email, phone)

        return CrmIntegrationService.create_and_integrate_crm(customer, server, currency, language)

    @classmethod
    def create_customer_with_test_period_licence(cls, name: str, type: str, full_name, email, phone):
        customer = Customer.objects.create(
            name=name, type=type,
            status=CustomerStatuses.TEST_DRIVE,
            crm_status=CrmStatuses.ACTIVE,
            contact_phone_number=phone,
            contact_person=full_name,
            contact_email=email,
            users_count=0,
        )

        license_entity, __ = LicenseEntity.objects.get_or_create(name='CRM')

        license_start = timezone.now().date()
        license_end = license_start + timezone.timedelta(days=14)

        License.objects.create(
            customer=customer,
            license_entity=license_entity,
            type=LicenseTypes.TESTING,
            start_date=license_start,
            end_date=license_end,
            price=0
        )

        return customer
