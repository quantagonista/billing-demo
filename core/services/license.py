import logging
from datetime import date
from decimal import Decimal
from typing import Optional

from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.db import transaction
from django.utils import timezone

from core.choices import LicenseTypes
from core.models import Customer, License, Invoice, LicenseInvoiceItem, LicenseEntity
from core.services import CrmIntegrationService, InvoiceService

logger = logging.getLogger("django")


class LicenseCheckService:
    @classmethod
    def check_all_licenses(cls):
        if not settings.CRM_LICENSE_UPDATING_ENABLED:
            logger.info("Licenses checking is DISABLED")
            return

        logger.info("Licenses checking started...")

        for customer in Customer.objects.all():
            cls.check_license_and_update_crm_for(customer)

        logger.info("Licenses checking finished")

    @classmethod
    def get_customer_license_end_date(cls, customer: Customer) -> Optional[date]:
        today = timezone.now().today().date()
        last_active_license = customer.licenses.filter(active=True, start_date__lte=today).order_by("-end_date").first()

        if last_active_license:
            return last_active_license.end_date

        return None

    @classmethod
    def check_license_and_update_crm_for(cls, customer: Customer):
        if not customer.server or not customer.org_id:
            logger.info(f"{customer} - Unable to update end license date: no organization ID or no server")
            return

        license_end_date = cls.get_customer_license_end_date(customer)
        license_end_date = license_end_date or date(2000, 1, 1)

        response = CrmIntegrationService.update_org_end_license_date(customer.server, customer.org_id, license_end_date)

        if response:
            logger.info(f"{customer} - updated license end date {license_end_date}")
        else:
            logger.info(f"{customer} - Unable to update end license date")


class LicenseService:
    @classmethod
    @transaction.atomic
    def create_license_with_first_invoice(cls, customer: Customer,
                                          license_entity: LicenseEntity,
                                          start_date: timezone.datetime,
                                          price: Decimal,
                                          first_payment: Decimal,
                                          first_payment_months: int,
                                          type_: LicenseTypes = LicenseTypes.MONTHLY):

        license_ = License.objects.create(
            customer=customer,
            license_entity=license_entity,
            start_date=start_date,
            end_date=start_date,
            price=price,
            type=type_,
        )

        license_end_date = start_date + relativedelta(months=first_payment_months)

        invoice = Invoice.objects.create(
            customer=customer,
            title=InvoiceService.get_title_for_invoice(start_date, license_end_date),
        )

        _ = LicenseInvoiceItem.objects.create(
            invoice=invoice,
            license=license_,
            start_date=start_date,
            end_date=license_end_date,
            title=InvoiceService.get_title_for_invoice_item(license_, start_date, license_end_date),
            initial_amount=first_payment,
            discount_amount=0
        )

        return license_, invoice

    @classmethod
    def disable_old_licenses(cls, customer: Customer):
        License.objects.filter(customer=customer).update(active=False)
