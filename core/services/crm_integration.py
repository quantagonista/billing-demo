import datetime
import logging
import uuid
from typing import Optional

import requests
from django.conf import settings
from django.db import transaction
from django.utils.translation import gettext_lazy as _

from core.constants import (
    CRM_SERVER_UNAVAILABLE_ERROR, INVALID_CUSTOMER_FULL_NAME,
    CUSTOMER_HAS_NO_SERVER, CRM_INTEGRATION_INCONSISTENT_ERROR,
    CUSTOMER_HAS_NO_INTEGRATION
)
from core.exceptions import CRMIntegrationError
from core.models import Customer, CustomUser, ExternalCrmIntegration, CrmServer
from core.models.choices import CrmStatuses

logger = logging.getLogger(__name__)


class CrmIntegrationService:
    base_url = settings.SITE_URL

    @classmethod
    @transaction.atomic
    def create_and_integrate_crm(cls,
                                 customer: Customer,
                                 server: CrmServer,
                                 currency: str = 'RUB',
                                 language: str = 'ru') -> Optional[str]:

        """ Makes API call to CRM to create Organization and initial CrmUser.
            Also creates ExternalCrmIntegration.
        """

        crm_integration = cls._create_external_crm_integration(customer, server)

        try:
            first_name, last_name = cls._split_full_name(customer.contact_person)

        except ValueError:
            raise CRMIntegrationError(INVALID_CUSTOMER_FULL_NAME)

        license_end_date = None
        if license_ := customer.get_current_license():
            license_end_date = license_.end_date.strftime("%Y-%m-%d")

        data = {
            'name': customer.name,
            'type': customer.type.code,
            'first_name': first_name,
            'last_name': last_name,
            'email': customer.contact_email,
            'admin_gateway_url': cls.base_url,
            'integration_token': crm_integration.value,
            'currency': currency,
            'language': language,
            'license_end_date': license_end_date
        }

        response = cls._make_post_request(
            url=f"{server.url}/api/organizations/",
            auth_token=server.auth_token, data=data)

        customer.org_id = response.get("org_id")
        customer.server = server
        customer.save()

        invitation_accept_url = response.get('invitation_accept_url', None)

        return invitation_accept_url

    @classmethod
    @transaction.atomic
    def create_crm_integration(cls, customer: Customer, server: CrmServer):
        """ Makes API call to integrate existing Organization """

        crm_integration = cls._create_external_crm_integration(customer, server)
        data = {
            'name': customer.name,
            'admin_gateway_url': cls.base_url,
            'integration_token': crm_integration.value
        }
        url = f"{server.url}/api/organizations/create-integration/"

        response = cls._make_post_request(url, data, server.auth_token)

        customer.org_id = response.get("org_id")
        customer.crm_status = CrmStatuses.ACTIVE
        customer.server = server
        customer.save()

    @classmethod
    def lock_crm_access_for(cls, customer: Customer):
        cls.update_org_end_license_date(
            customer.server, customer.org_id,
            datetime.datetime.now().date(),
            raise_exception=True
        )

    @classmethod
    @transaction.atomic
    def update_org_end_license_date(cls,
                                    server: CrmServer,
                                    org_id: int,
                                    license_end_date: Optional[datetime.date],
                                    raise_exception=False):

        url = f"{server.url}/api/organization/{org_id}/license/"
        data = {
            'license_end_date': license_end_date.strftime("%Y-%m-%d"),
        }
        try:
            return cls._make_post_request(url, data, server.auth_token)

        except CRMIntegrationError as e:
            logger.error(e.message, exc_info=e)

            if raise_exception:
                raise e

    @classmethod
    @transaction.atomic()
    def delete_crm(cls, customer: Customer):
        if not customer.server:
            raise CRMIntegrationError(CUSTOMER_HAS_NO_SERVER)

        integration = customer.get_crm_integration()
        if not integration:
            raise CRMIntegrationError(CUSTOMER_HAS_NO_INTEGRATION)

        crm_url = customer.server.url
        if crm_url != integration.crm_url:
            raise CRMIntegrationError(CRM_INTEGRATION_INCONSISTENT_ERROR)

        auth_token = customer.server.auth_token
        url = f"{crm_url}/api/organization/{customer.org_id}/delete/"
        data = {
            'id': customer.org_id,
            'integration_token': integration.value,
        }

        integration.delete()
        customer.crm_status = CrmStatuses.DELETED
        customer.remove_server()

        return cls._make_post_request(url, data, auth_token)

    @classmethod
    def _create_external_crm_integration(cls,
                                         customer: Customer,
                                         server: CrmServer):
        uuid_string = str(uuid.uuid4())

        internal_user = CustomUser(
            username=f"{customer.name}-{uuid_string}",
            customer=customer
        )
        internal_user.set_password(uuid_string)
        internal_user.save()

        return ExternalCrmIntegration.objects.create(user=internal_user,
                                                     crm_url=server.url)

    @classmethod
    def _make_post_request(cls, url: str, data: dict, auth_token: str) -> dict:
        try:
            logger.info(f"POST: {url}")
            response = requests.post(
                url, json=data,
                                     headers={'Authorization': auth_token})
            logger.info(f"Response: {response.status_code}: {response.text}")
        except Exception as e:
            logger.error(CRM_SERVER_UNAVAILABLE_ERROR, exc_info=e)
            raise CRMIntegrationError(CRM_SERVER_UNAVAILABLE_ERROR)

        if not response.status_code == 200:
            cls._handle_error_response(response)

        return response.json()

    @classmethod
    def _handle_error_response(cls, response):
        try:
            message = str(response.json())

        except:
            message = response.text[:20]

        err_msg = "CRM Server:  {error}. {status} - {message}".format(
            error=_('Ошибка интеграции'),
            status=response.status_code,
            message=message)
        logger.error(err_msg)

        raise CRMIntegrationError(err_msg)

    @classmethod
    def _split_full_name(cls, full_name: str):
        splitted_name = str(full_name).split(' ', maxsplit=1)

        if len(splitted_name) == 1:
            splitted_name.append('')

        return splitted_name
