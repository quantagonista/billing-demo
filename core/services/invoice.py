import logging
from datetime import timedelta, datetime
from decimal import Decimal

from django.db import transaction
from django.utils import timezone
from django.utils.translation import gettext as _

from core import models, exceptions
from core.choices import LicenseTypes
from core.models import LicenseInvoiceItem, License
from core.models.choices import CustomerStatuses

logger = logging.getLogger("django")


class InvoiceService:
    model = models.Invoice

    @classmethod
    @transaction.atomic
    def pay_invoice_manually(cls, invoice: models.Invoice, payment_method: models.PaymentMethod, amount: Decimal, payment_date):
        """
        returns has_paid: bool - successfully paid for invoice
        """
        debit_transaction = models.Transaction.objects.create(
            customer=invoice.customer,
            payment_method=payment_method,
            type=models.Transaction.DEBIT,
            amount=amount,
        )
        models.Transaction.objects.filter(id=debit_transaction.id).update(created_at=payment_date, updated_at=payment_date)

        if models.Transaction.objects.get_customer_balance(invoice.customer) >= invoice.final_amount:
            credit_transaction = models.Transaction.objects.create(
                payment_method=payment_method,
                customer=invoice.customer,
                invoice=invoice,
                type=models.Transaction.CREDIT,
                amount=invoice.final_amount,
            )
            models.Transaction.objects.filter(id=credit_transaction.id).update(created_at=payment_date, updated_at=payment_date)
            invoice.mark_as_paid(payment_date)
            return True

        return False

    @classmethod
    def create_invoice(cls, license: models.License,
                       initial_amount: Decimal, discount_amount: Decimal,
                       start_date: datetime, end_date: datetime,
                       description: str = None):

        if models.License.objects.overlapping_licenses(license.customer, license.license_entity, start_date, end_date, license.id).exists():
            raise exceptions.LicensePeriodIntersectionError()

        invoice = models.Invoice.objects.create(
            title=cls.get_title_for_invoice(start_date, end_date),
            customer=license.customer,
            initial_amount=initial_amount,
            discount_amount=discount_amount,
        )

        models.LicenseInvoiceItem.objects.create(
            title=cls.get_title_for_invoice_item(license, start_date, end_date),
            invoice=invoice,
            license=license,
            start_date=start_date,
            end_date=end_date,
            initial_amount=initial_amount,
            discount_amount=discount_amount,
            description=description or license.description,
        )

        return invoice

    @classmethod
    @transaction.atomic()
    def create_new_for_license(cls, license_: models.License):
        discount_amount = cls._get_discount_amount(license_)

        start_date, end_date = cls._get_new_license_date_range(license_)

        if models.License.objects.overlapping_licenses(license_.customer, license_.license_entity, start_date, end_date, license_.id).exists():
            raise exceptions.LicensePeriodIntersectionError(f"Cannot prolong license #{license_.id} for {license_.customer}")

        invoice = models.Invoice.objects.create(
            title=cls.get_title_for_invoice(start_date, end_date),
            customer=license_.customer,
            initial_amount=license_.price,
            discount_amount=discount_amount
        )

        models.LicenseInvoiceItem.objects.create(
            title=cls.get_title_for_invoice_item(license_, start_date, end_date),
            invoice=invoice,
            license=license_,
            start_date=start_date,
            end_date=end_date,
            initial_amount=license_.price,
            discount_amount=discount_amount,
            description=license_.description
        )

        return invoice

    @classmethod
    def prolong_invoice_period(cls, invoice_item: LicenseInvoiceItem):
        if invoice_item.invoice.is_paid:
            return

        invoice_item.end_date += invoice_item.license.length
        invoice_item.initial_amount += invoice_item.license.price

        invoice_item.save()

    @classmethod
    def get_title_for_invoice(cls, start, end):
        return f"CRM/LMS: {start.strftime('%d %b')} - {end.strftime('%d %b')}"

    @classmethod
    def get_title_for_invoice_item(cls, license: License, start: datetime, end: datetime):
        return (
            f"{_('Подписка за')} {license.license_entity.name} "
            f"{start.strftime('%d %b')} - {end.strftime('%d %b')}"
        )

    @classmethod
    def _get_discount_amount(cls, license: models.License):
        return Decimal(license.price) / Decimal(100) * Decimal(license.discount_percentage)

    @classmethod
    def _get_new_license_date_range(cls, license: models.License):
        start_date = license.end_date
        last_invoice_item = (
            license.invoice_items
            .instance_of(LicenseInvoiceItem)
            .order_by("end_date")
            .last()
        )

        if last_invoice_item:
            start_date = last_invoice_item.end_date

        end_date = start_date + license.length

        return start_date, end_date

    @classmethod
    @transaction.atomic()
    def generate_invoices_for_ending_licences(cls, current_timestamp=timezone.now(), days_threshold=7):
        in_a_week = current_timestamp.date() + timedelta(days=days_threshold)
        created_count = updated_count = 0

        # Take active licenses ending in a week or before (already ended)
        licenses = (
            License.objects
            .filter(active=True, customer__status=CustomerStatuses.ACTIVE, end_date__lte=in_a_week)
            .exclude(type=LicenseTypes.TESTING)
        )

        for license in licenses:
            invoice_item = (
                LicenseInvoiceItem.objects
                .filter(license=license, start_date__gte=license.end_date)
                .select_related('invoice')
                .first()
            )

            # If there is no invoices already created - create one
            if not invoice_item:
                try:
                    cls.create_new_for_license(license)
                    created_count += 1
                except Exception as e:
                    logger.warning(
                        f"Unable to create invoice for licence {license}", exc_info=e
                    )

                continue

            # If invoice and it is not paid - prolong invoice

            invoice_is_not_paid_yet = not invoice_item.invoice.is_paid
            time_for_prolong_has_come = invoice_item.end_date <= in_a_week
            should_prolong = invoice_is_not_paid_yet and time_for_prolong_has_come

            if should_prolong:
                cls.prolong_invoice_period(invoice_item)
                updated_count += 1

        return created_count, updated_count
