import os
from pathlib import Path

from dotenv import load_dotenv

load_dotenv()

BASE_DIR = Path(__file__).resolve().parent.parent

SECRET_KEY = os.getenv('DJANGO_SECRET_KEY', 'test-secret-key')

DEBUG = str(os.getenv('DEBUG', 'false')).lower() == 'true'

ALLOWED_HOSTS = ['*']

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'core.apps.CoreConfig',
    'payments.apps.PaymentsConfig',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'config.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'config.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.getenv('POSTGRES_DB', 'billing_db'),
        'USER': os.getenv('POSTGRES_USER', 'billing_user'),
        'PASSWORD': os.getenv('POSTGRES_PASSWORD', 'billing_pass'),
        'HOST': os.getenv('POSTGRES_HOST', 'postgres'),
        'PORT': os.getenv('POSTGRES_PORT', '5432'),
    }
}

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

AUTH_USER_MODEL = 'core.CustomUser'

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

USE_TZ = True
TIME_ZONE = 'UTC'

# Localization
USE_I18N = True

LANGUAGE_CODE = 'en-us'

LOCALE_PATHS = (
    os.path.join(BASE_DIR, "locale"),
)
FORMAT_MODULE_PATH = ['core.formats']

LANGUAGES = [
    ('ru', 'Russian'),
    ('en', 'English'),
]

DATE_FORMATS = {
    'JS_DATE': {
        'ru': 'dd.mm.yyyy',
        'en': 'yyyy-mm-dd'
    },
    'JS_DATE_TIME': {
        'ru': 'dd.mm.yyyy HH:MM',
        'en': 'yyyy-mm-dd HH:MM'
    },
    'JS_PICKER_DATE': {
        'ru': 'dd.mm.yy',
        'en': 'yy-mm-dd'
    },
}

# Payments

PAYBOX_MERCHANT_ID = os.getenv('PAYBOX_MERCHANT_ID', 'test_paybox_merchant_id')
PAYBOX_PRIVATE_KEY = os.getenv('PAYBOX_PRIVATE_KEY', 'test_paybox_private_key')
PAYBOX_TEST_MODE = os.getenv('PAYBOX_TEST_MODE', True)

STRIPE_API_KEY = os.getenv('STRIPE_API_KEY', 'test_stripe_api_key')
STRIPE_API_VERSION = os.getenv('STRIPE_API_VERSION', '2020-08-27')
STRIPE_WEBHOOK_SECRET = os.getenv('STRIPE_WEBHOOK_SECRET', 'test_stripe_webhook_secret')

# Needed for payment related services
SITE_URL = os.getenv('SITE_URL', "http://localhost:8000")


# CORS Settings
CORS_ORIGIN_ALLOW_ALL = False
CORS_ORIGIN_WHITELIST = (
    'http://localhost:8000',
)

CORS_ALLOW_METHODS = [
    'GET',
    'OPTIONS',
    'POST',
]

CORS_ALLOW_HEADERS = [
    'accept',
    'accept-encoding',
    'authorization',
    'content-type',
    'dnt',
    'origin',
    'user-agent',
    'x-csrftoken',
    'x-requested-with',
]


# Static
STATIC_URL = '/static/'
MEDIA_URL = "/media/"

STATICFILES_DIRS = [
    'core/static',
]

STATIC_ROOT = os.path.join(BASE_DIR, "static")
MEDIA_ROOT = os.path.join(BASE_DIR, "media")


# Sentry - is turned off for demo purposes

# SENTRY_DSN = os.environ.get("SENTRY_DSN", "")
# if SENTRY_DSN:
#     sentry_sdk.init(
#         dsn=SENTRY_DSN,
#         integrations=[DjangoIntegration()],
#         environment='billing',
#         traces_sample_rate=0.5,
#         send_default_pii=True
#     )