from django.contrib import admin
from django.urls import path, include
from django.views.i18n import JavaScriptCatalog

from core.views.main import HomeView
from payments.views.instant_payment import InstantPayView

urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('', include('core.urls')),

    path('admin/', admin.site.urls),

    path('api/', include('core.api.urls')),
    path('payments/', include('payments.urls')),
    path('accounts/', include("auth.urls")),

    path('instant-pay/', InstantPayView.as_view(), name='instant-pay'),

    path("i18n/", include("django.conf.urls.i18n")),
    path('jsi18n/', JavaScriptCatalog.as_view(packages=['core']), name='javascript-catalog'),
]
