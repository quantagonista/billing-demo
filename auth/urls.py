from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import (
    PasswordResetView, PasswordResetConfirmView, PasswordResetDoneView,
    PasswordResetCompleteView
)
from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from auth.views import (
    logout, ChangePasswordView, AjaxPasswordResetView,
    AjaxPasswordResetDoneView, CrmLoginView, CrmLoginByTokenView,
    BackToCrmView
)

urlpatterns = [
    path('logout/', logout, name="logout"),
    path('change-password/', login_required(ChangePasswordView.as_view()), name="change-password"),
    path('reset-password/', PasswordResetView.as_view(), name="reset_password"),
    path('reset-password-ajax/', AjaxPasswordResetView.as_view(), name="reset_password_ajax"),
    path('reset-password-confirm/<uidb64>/<token>/', PasswordResetConfirmView.as_view(), name="password_reset_confirm"),
    path('reset-password-done/', PasswordResetDoneView.as_view(), name="password_reset_done"),
    path('reset-password-done-ajax/', AjaxPasswordResetDoneView.as_view(), name="password_reset_done_ajax"),
    path('reset-password-complete/', PasswordResetCompleteView.as_view(), name="password_reset_complete"),
    path('login/', CrmLoginView.as_view(), name="login"),
    path('login-from-crm/', csrf_exempt(CrmLoginByTokenView.as_view())),
    path('back-to-crm/', login_required(BackToCrmView.as_view()), name="back_to_crm"),
]
