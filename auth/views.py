from django.conf import settings
from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.views import (
    auth_logout, LoginView, PasswordResetView, PasswordResetDoneView
)
from django.http import Http404
from django.shortcuts import redirect, render
from django.urls import reverse, reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views import View
from django.views.generic import FormView

from auth.constants import REDIRECT_URL_SESSION_KEY
from common.views import LoginRequiredView
from core.models import ExternalCrmIntegration


def logout(request):
    auth_logout(request)
    return redirect("/")


class CrmLoginView(LoginView):
    def get_success_url(self):
        return reverse('customer-list') if self.request.user.is_superuser else reverse("home")

    def get_redirect_url(self):
        if self.request.user.is_authenticated and hasattr(self.request.user, 'one_time_password'):
            return reverse("change-password")
        return super().get_redirect_url()


class CrmLoginByTokenView(View):
    def post(self, request, *args, **kwargs):
        token = request.POST.get("token")

        if token:
            token_object = ExternalCrmIntegration.objects.filter(value=token).first()
            if token_object:
                login(request, token_object.user)
                response = redirect("/")
                self.set_lang(response)
                self.set_redirect_url()
                return response

        raise Http404()

    def set_lang(self, response):
        lang_code = self.request.POST.get("lang")

        if not (lang_code and lang_code in ('ru', 'en')):
            lang_code = 'en'

        response.set_cookie(
            settings.LANGUAGE_COOKIE_NAME, lang_code,
            max_age=settings.LANGUAGE_COOKIE_AGE,
            path=settings.LANGUAGE_COOKIE_PATH,
            domain=settings.LANGUAGE_COOKIE_DOMAIN,
            secure=settings.LANGUAGE_COOKIE_SECURE,
            httponly=settings.LANGUAGE_COOKIE_HTTPONLY,
            samesite=settings.LANGUAGE_COOKIE_SAMESITE,
        )

    def set_redirect_url(self):
        redirect_url = self.request.POST.get("redirect_url")
        self.request.session[REDIRECT_URL_SESSION_KEY] = redirect_url


class BackToCrmView(View):
    def dispatch(self, request, *args, **kwargs):
        redirect_url = self.get_redirect_url()

        if not redirect_url:
            redirect_url = "/"

            if hasattr(request.user, "x_integration"):
                redirect_url = request.user.x_integration.crm_url

        logout(request)
        return redirect(redirect_url)

    def get_redirect_url(self):
        return self.request.session.get(REDIRECT_URL_SESSION_KEY)


class ChangePasswordView(LoginRequiredView, FormView):
    template_name = "registration/change_pass_form.html"
    form_class = PasswordChangeForm

    def form_valid(self, form):
        form.user.one_time_password = False
        form.save()
        login(self.request, form.user)
        messages.success(self.request, _("Пароль успешно изменен"))
        return redirect("/")

    def form_invalid(self, form):
        return render(self.request, self.template_name, {"form": form})


class AjaxPasswordResetView(PasswordResetView):
    success_url = reverse_lazy('password_reset_done_ajax')


class AjaxPasswordResetDoneView(PasswordResetDoneView):
    template_name = 'registration/password_reset_done_ajax.html'
