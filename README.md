# Simple Billing CRM

## Overview
Simple Billing CRM is a Django-based application designed for managing customers and their invoices for licenses. 
It provides functionalities to create, read, update, and delete customer data and associated invoices.
Also it has some scaffolds for integrations with payment providers (Stripe, Paybox)

## Getting Started

### Prerequisites
- Docker + Compose

### Setting up the Project
1. **Clone the Repository**: Clone this repository to your local machine.
2. **Navigate to the Project Directory**: `cd billing-demo`
3. **Copy .env.template and setup env variables described below**: `cp .env.template .env`

### Running the Application
To run the application, use the following command: `docker-compose up --build -d`

### Running Tests
Tests can be run using a special Docker Compose configuration. 
To execute tests, run:

```docker-compose -f docker-compose-tests.yml up --build```



## Environment Variables
This project uses environment variables for configuration. Below is a table of the required environment variables:

| Variable            | Description                           | Example           |
|---------------------|---------------------------------------|-------------------|
| `DJANGO_SECRET_KEY` | Secret key for Django application     | `test-secret-key` |
| `DEBUG`             | Debug mode for Django (True or False) | `True`            |
| `POSTGRES_HOST`     | URL for the PostgreSQL database       | `postres`         |
| `POSTGRES_PORT`     | Database TCP port                     | `5432`            |
| `POSTGRES_USER`     | Database user                         | `billing_user`    |
| `POSTGRES_PASS`     | Database password                     | `billing_pass`    |
| `POSTGRES_NAME`     | Database name                         | `billing_db`      |


## To-Do
- [x] Implement core logic for Customers, Invoices and Licenses
- [x] Integrate Payment Gateway
- [x] Cover critical parts of core logic with tests
- [ ] Write Integration Tests for Payment Providers (optional, It becomes outdated to soon)

